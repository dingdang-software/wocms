<?php


define("APP_NAME","WO-CMS");
define("APP_VERSION","1.0.0");


define("DEBUG",true); //生产环境设置为false


define("DIR_ROOT", __DIR__ . "/../");
define("DIR_SRC", DIR_ROOT . "src/");
define("DIR_ROUTES", DIR_SRC . "routes/");
define("DIR_TEMPLATES", DIR_SRC . "templates/");
