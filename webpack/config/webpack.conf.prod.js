const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const babelpolyfill = require("babel-polyfill");
module.exports = {
    entry: [
        path.resolve(__dirname, '../src/index.js')
    ], //指定入口文件，程序从这里开始编译,__dirname当前所在目录, ../表示上一级目录, ./同级目录
    output: {
        path: path.resolve(__dirname, '../dist'), // 输出的路径
        filename: 'app/[name]_[hash:8].js',  // 打包后文件
        publicPath:'./'
    },
    module: {
        rules: [
            {
                test: /\.(css|scss)$/,
                loader: ["style-loader", "css-loader", "sass-loader"]
            },
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(png|jpg|gif)$/,
                loader: "file-loader?name=images/[name].[ext]"
            }
        ]
    },
    mode: 'production',
    //devtool: "source-map",
    plugins: [
        new HtmlWebpackPlugin({
            title: 'wo-cms后台管理系统',
            favicon:path.resolve(__dirname, '../src/asset/wocms-logo.png'),
            template: path.resolve(__dirname, '../src/index.template.html'),
            inject: true
        }),
    ]
};
