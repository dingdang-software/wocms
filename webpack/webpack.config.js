var webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    // 配置入口
    entry: {
        main :  './src/script/home.js',
    },
    // 配置出口
    output: {
        path: __dirname + "/../public/assets",
        filename: 'app.js',
        publicPath: '/',
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.(css|scss)$/,
                loader: [ MiniCssExtractPlugin.loader,"css-loader", "sass-loader"]
                // use: [
                //
                //      MiniCssExtractPlugin.loader,
                //     'css-loader',
                // ],
            },
            {
                test: /\.(js|jsx)$/,
                use: {
                    loader: 'babel-loader'
                },
                exclude: /node_modules/
            },
            {
                test: /\.(png|jpg|gif)$/,
                loader: "file-loader?name=[name].[ext]"
            }

        ]
    },
    mode: 'development',
    plugins: [
        new webpack.ProvidePlugin({
            $:"jquery",
            jQuery:"jquery",
            "window.jQuery":"jquery"

        }),
        new CleanWebpackPlugin(
            [__dirname+'/../public/assets/*'],　     //匹配删除的文件
            {
                root: __dirname,       　　　　　　　　　　//根目录
                verbose: true,        　　　　　　　　　　//开启在控制台输出信息
                dry: false        　　　　　　　　　　//启用删除文件
            }
        ),
        new MiniCssExtractPlugin({
            // 类似 webpackOptions.output里面的配置 可以忽略
            filename: 'app.css',
            chunkFilename: 'app.css',
        })
    ],
    // 起本地服务，我起的dist目录
    devServer: {
        contentBase: "./dist/",
        historyApiFallback: true,
        inline: true,
        hot: true,
        host: '127.0.0.1',//我的局域网ip
    }
}