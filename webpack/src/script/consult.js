require('../styles/consult.scss');
$(function () {
    let mailReg=/^[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*@([a-zA-Z0-9]+[-.])+([a-z]{2,5})$/ims;
    $(".submitConsult").click(function () {
        let consultEmail=$(".consultEmail").val();
        let consultPhone=$(".consultPhone").val();
        let consultName=$(".consultName").val();
        let consultRequirement=$(".consultRequirement").val();
        if((!consultEmail&&!consultPhone)||!consultName||!consultRequirement){
            alert('请确认您输入数据不为空')
        }
        else {
            let canRequest=false;
            if(consultEmail&&mailReg.test(consultEmail)){
                canRequest=true;
            }
            if(consultPhone){
                canRequest=true;
            }
            if(canRequest){
                alert('我们已经收到您的需求');
                $.ajax({
                    type:'POST',
                    url:serverUrl+"consult/add",
                    data:{c_email:consultEmail,c_phone:consultPhone,c_name:consultName,c_requirement:consultRequirement},
                    success(e){
                        console.log(e);
                    },
                    error(e) {

                    }
                });
            }
            else {
                alert('邮箱格式错误');
            }


        }

    })



});