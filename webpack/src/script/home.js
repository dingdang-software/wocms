import 'bootstrap/dist/js/bootstrap.js';
require('bootstrap/dist/css/bootstrap.css');
import './nav.js';
import './footer.js';
import './articleDetail.js';
import './recruit.js';
import './articleList.js';
import './consult.js';
require('../styles/home.scss');

function setCarouselContent (){
    // let carouselHtml='',carouselPointHtml='';
    // for( let index in data){
    //     carouselHtml+=`<div class="carousel-item ${index==0?'active':''}">
    //                 <a href="${data[index].link_url}"><img class="d-block w-100" src="${data[index].image}" ></a>
    //             </div>`
    //     carouselPointHtml+=`<li data-target="#carouselExampleIndicators" data-slide-to="${index}" class="carouselPoint ${index==0?'active':''}"></li>`
    // }
    // $('#homeCarousel .carousel-inner').html(carouselHtml);
    // $('#homeCarousel .carousel-indicators').html(carouselPointHtml);
    $('#homeCarousel>.carousel').carousel({
        interval: 3000
    })
}


function setSeamlessScrolling() {
    var content = document.getElementsByClassName("seamlessScrolling")[0];
    var ul = content.getElementsByTagName("ul")[0];
    var li = ul.getElementsByTagName("li")
    var speed = 1;

    ul.innerHTML += ul.innerHTML;
    ul.style.width = li.length * li[0].offsetWidth + "px"

    function run() {
        if (ul.offsetLeft < (-ul.offsetWidth / 2)) {

            ul.style.left=0;
        } else if (ul.offsetLeft > 0) {
            ul.style.left = -ul.offsetWidth / 2 + "px"
        }

        ul.style.left = ul.offsetLeft - speed + "px"
    }
    let timer = setInterval(run, 30)

    content.onmouseover = function() {
        clearInterval(timer)
    }

    content.onmouseout = function() {
        timer = setInterval(run, 30)
    }
}

let requestUrl='http://localhost:9090';

$(function () {

     if($('#homeCarousel').length>0){
         setCarouselContent();
         console.log("有幻灯片");
    //     $.ajax({
    //         type:'GET',
    //         url:requestUrl+"/slide/get",
    //         success(e){
    //             if(e.success==true){
    //                 setCarouselContent(e.slideList);
    //             }
    //
    //         }
    //     });
     }

     if($('.seamlessScrolling').length>0){
         console.log('有无缝滚动')
         setSeamlessScrolling();
     }
});



