import React from 'react';
import { render } from 'react-dom';
import App from './components/App';



const renderDom = Component => {
    render(
            <Component />
        ,
        document.getElementById('app')
);
};
renderDom(App);

if (module.hot) {
    module.hot.accept('./components/App', () => {
        const App = require('./components/App').default;
        renderDom(App);
    })
}