import {
    message, Button, Divider
} from 'antd';
import React from 'react';
import TableOperation from "../public/TableOperation";
import connect from "react-redux/es/connect/connect";
import {setColumns, setEditingKey, setSelectArr, changeTableParams, setReqUrl} from "../public/Reducer";
import {requestGet, requestPost} from '../public/Request';
import {TableEditCustom} from "../public/TableEditCustom";

@connect(
    //需要的属性
    state => state,
    //需要的方法，自动dispatch
    {setColumns, setEditingKey, setSelectArr, changeTableParams, setReqUrl}
)
class RecruitTypes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {data: [], editingKey: '', addRows: []};
        this.columns = [
            {
                title: 'id',
                dataIndex: 'id',
                sorter: true,
                width: '20%',
                editable: true,
            },
            {
                title: '招聘类型',
                dataIndex: 'o_type',
                width: '25%',
                editable: true,
            },
            {
                title: '招聘数量',
                dataIndex: 'cnt',
                width: '25%',
                editable: true,
            },

            {
                title: '操作',
                dataIndex: 'operation',
                render: (text, record) => {
                    return (
                        <TableOperation record={record} save={this.save}
                                        handleDelete={this.handleDelete}>

                        </TableOperation>
                    )
                }
            },
        ];
    }

    isEditing = record => record.id === this.props.table.editingKey;


    save = (form, key) => {

        form.validateFields(async (error, row) => {
            if (error) {
                return;
            }
            const newData = [...this.props.table.rows];
            let isAddRow = false;
            const index = newData.findIndex(item => key === item.id);
            if (index > -1) {
                const item = newData[index];
                for (let i in this.state.addRows) {
                    if (this.state.addRows[i] == key) {
                        isAddRow = true;
                    }
                }
                let postUrl = isAddRow ? 'recruit-types/add' : 'recruit-types/update';
                let params = {
                    id: key,
                    o_type: row.o_type,
                    cnt: row.cnt
                };
                let resultData = await requestPost(postUrl, params);
                if (resultData.success == true) {
                    message.success('修改成功');
                    row.id = resultData.data;
                    newData.splice(index, 1, {
                        ...item,
                        ...row,
                    });
                    this.props.setEditingKey('');
                    this.props.changeTableParams({rows: newData});
                } else {
                    message.success('修改失败');
                }

            } else {
                newData.push(row);
                this.props.setEditingKey('');
                this.props.changeTableParams({rows: newData});
            }
        });
    }


    handleAdd = () => {
        const data = this.props.table.rows;
        const newData = {
            id: Math.random(),
            o_type: '请输入招聘类型',
            cnt: `请输入招聘数量`,
        };
        const addRows = this.state.addRows;
        addRows.push(newData.id)
        this.setState({addRows});
        this.props.changeTableParams({rows: [...data, newData]});
    }

    handleDelete = async (key) => {
        const data = [...this.props.table.rows];
        let params = {id: key}
        let resultData = await requestPost('recruit-types/delete', params);
        console.log(resultData);
        if (resultData.success) {
            message.success('删除成功');
            this.props.changeTableParams({rows: data.filter(item => item.id !== key)});
        } else {
            message.error('删除失败');
            for (let i in this.state.addRows) {
                if (this.state.addRows[i] == key) {
                    this.props.changeTableParams({rows: data.filter(item => item.id !== key)});
                }
            }
        }
    }


    async componentDidMount() {
        await this.props.setReqUrl("recruit-types/list");
        let {
            changeTableParams,
            table: {
                page,
                order
            }
        } = this.props;

        let params = {page, order}
        let data = await requestGet('recruit-types/list', params);
        if (data) {
            params.total = data.total;
            params.rows = data.data;
        } else {
            message.error("请求数据失败")
        }

        params.loading = false;
        changeTableParams && changeTableParams(params);

        const columns = this.columns.map((col) => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    inputtype: col.dataIndex === 'cnt' ? 'number' : 'text',
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: col.dataIndex === 'id' ? '' : this.isEditing(record),
                }),
            };
        });
        this.props.setColumns(columns);
    }

    render() {

        return (
            <div>
                <h3>招聘分类信息</h3>
                <Divider>

                </Divider>
                <Button onClick={this.handleAdd} type="primary" style={{marginBottom: 16}}>
                    添加一个招聘类型
                </Button>
                <TableEditCustom >
                </TableEditCustom>

            </div>

        );
    }
}

export default RecruitTypes;