import {Form, Button, message,Divider} from 'antd';
import React from 'react';
const FormItem = Form.Item;

import CKEditorCustom from "../public/CKEditorCustom";
import {requestGet, requestPost} from "../public/Request";


class AboutForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            content:'',
        };
        this.handleSubmit=this.handleSubmit.bind(this);
        this.onChange=this.onChange.bind(this);

    }


//文章修改回调
    onChange(content){
        this.props.form.setFieldsValue({content:content})

    }


    handleSubmit (e) {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll(async (err, values) => {
            if (!err) {
                console.log(values);
                let params={
                    id:this.state.id,
                    content: values.content,
                };
                let resultData= await requestPost('about/update',params);
                if(resultData){
                    message.success('修改成功');

                }
            }
        });
    }

    componentDidMount=async ()=> {


        //获得某文章的数据

        let params={};
        let resultData= await requestGet('about',params);
            if(resultData){
                let data=resultData;
                this.props.form.setFieldsValue({
                    content:data.content
                })
                this.setState({id:data.id});

            }

    }


    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 3 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 20 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 10,
                },
            },
        };


        return (

            <div>

                <h3>关于我们</h3>
                <Divider>

                </Divider>
                <Form onSubmit={this.handleSubmit}>

                    <FormItem
                        {...formItemLayout}
                        label="关于我们内容"
                    >
                        {getFieldDecorator('content')(
                            <CKEditorCustom  onChange={this.onChange} content={this.props.form.getFieldValue('content')}>

                            </CKEditorCustom>
                        )}
                    </FormItem>

                    <FormItem {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit">添加</Button>
                    </FormItem>
                </Form>
            </div>

        );
    }
}

const About = Form.create()(AboutForm);
export default About;