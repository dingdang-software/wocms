import {
    Form,
    Input,
    Button,
    message,
    Divider
} from 'antd';
import React from 'react';
const FormItem = Form.Item;
import UploadCustom from "../public/UploadCustom";
import {requestGet, requestPost} from "../public/Request";


class CompanyForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            loading: false,
            imgUrl:''
        };
        this.handleSubmit=this.handleSubmit.bind(this);
    }

    handleSubmit (e) {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll(async (err, values) => {
            if (!err) {
                let params={
                    id:this.state.id,
                    c_name: values.c_name,
                    c_addr:values.c_addr,
                    tel:values.tel,
                    logo:values.logo,
                    icp: values.icp
                };
                let resultData= await requestPost('company/update',params);
                if(resultData){
                    message.success('修改成功');

                }
            }
        });
    }


    handleChange =(imageUrl)=> {

        this.props.form.setFieldsValue({logo:imageUrl});

    }


     async componentWillMount() {
        let params={};
        let resultData= await requestGet('company',params);
        if(resultData){
            let data=resultData;
            this.props.form.setFieldsValue({
                c_name:data.c_name,
                c_addr:data.c_addr,
                tel:data.tel,
                icp:data.icp,
                logo:data.logo
            });
            this.setState({id:data.id});

        }


    }

    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 3 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 10 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 6,
                },
            },
        };


        return (
            <Form onSubmit={this.handleSubmit}>
                <h3>公司信息</h3>
                <Divider ></Divider>
                <FormItem
                    {...formItemLayout}
                    label="公司Logo"
                >
                    {getFieldDecorator('logo')(
                        <UploadCustom uploadSuccess={this.handleChange} imageUrl={this.props.form.getFieldValue('logo')}></UploadCustom>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="公司名字"
                >
                    {getFieldDecorator('c_name', {
                        rules: [{ required: true, message: '请输入公司名!', whitespace: true }],
                    })(
                        <Input />
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="公司地址"
                >
                    {getFieldDecorator('c_addr')(
                        <Input />
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="公司电话"
                >
                    {getFieldDecorator('tel'
                    )(
                        <Input style={{ width: '100%' }}  />
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="网站ICP备案信息"
                >
                    {getFieldDecorator('icp')(
                        <Input />
                    )}
                </FormItem>
                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">修改</Button>
                </FormItem>
            </Form>
        );
    }
}

const Company = Form.create()(CompanyForm);
export default Company;