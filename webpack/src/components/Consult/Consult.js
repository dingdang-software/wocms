import {message, Divider} from 'antd';
import React from 'react';
import {TableCustom} from "../public/TableCustom";
import connect from "react-redux/es/connect/connect";
import {setColumns, setEditingKey, setSelectArr, changeTableParams, setReqUrl} from "../public/Reducer";
import {requestGet, requestPost} from '../public/Request';


@connect(
    //需要的属性
    state => state,
    //需要的方法，自动dispatch
    {setColumns, setEditingKey, setSelectArr, changeTableParams, setReqUrl}
)


class Consult extends React.Component {
    constructor(props) {
        super(props);
        this.state = {data: [], editingKey: '', addRows: []};
        this.columns = [
            {
                title: 'id',
                dataIndex: 'id',
                sorter: true,
                width: '10%',
                editable: false,
            },
            {
                title: '客户邮件地址',
                dataIndex: 'c_email',
                width: '15%',
                editable: true,
            },
            {
                title: '客户联系电话',
                dataIndex: 'c_phone',
                width: '15%',
                editable: true,
            },
            {
                title: '客户名字',
                dataIndex: 'c_name',
                width: '10%',
                editable: true,
            },
            {
                title: '客户需求描述',
                dataIndex: 'c_requirement',
                width: '40%',
                editable: true,
            },
            {
                title: '操作',
                dataIndex: 'operation',
                render: function (text, record, index) {
                    return <div><a href={"javascript:"}
                                   onClick={() => {
                                       this.handleDelete(record.id)
                                   }}
                                   id={record.id}>删除</a></div>
                }.bind(this)
            },
        ];
    }

    isEditing = record => record.id === this.props.table.editingKey;


    handleDelete = async (key) => {
        const data = [...this.props.table.rows];
        let params = {id: key}
        let resultData = await requestPost('consult/delete', params);
        console.log(resultData);
        if (resultData.success) {
            message.success('删除成功');
            this.props.changeTableParams({rows: data.filter(item => item.id !== key)});
        } else {
            message.error('删除失败');
            for (let i in this.state.addRows) {
                if (this.state.addRows[i] == key) {
                    this.props.changeTableParams({rows: data.filter(item => item.id !== key)});
                }
            }
        }
    }


    async componentDidMount() {

        await this.props.setReqUrl("consult/list");
        let {
            changeTableParams,
            table: {
                page,
                order
            }
        } = this.props;

        let params = {page, order}
        let data = await requestGet('consult/list', params);
        if (data) {
            params.total = data.total;
            params.rows = data.data;
        } else {
            message.error("请求数据失败")
        }

        params.loading = false;
        changeTableParams && changeTableParams(params);

        const columns = this.columns.map((col) => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    inputtype: 'text',
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                }),
            };
        });
        this.props.setColumns(columns);
    }

    render() {


        return (
            <div>
                <h3>咨询信息</h3>
                <Divider></Divider>
                <TableCustom>
                </TableCustom>
            </div>

        );
    }
}

export default Consult;