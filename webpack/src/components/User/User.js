import React from 'react';
import {Link} from 'react-router-dom';

import {
    Divider, message, Button, Popconfirm,
} from 'antd';
import ModalCustom from "../public/ModalCustom";
import {TableCustom} from "../public/TableCustom";
import connect from "react-redux/es/connect/connect";
import {setColumns, setVisible, setRichText, changeTableParams, setReqUrl} from "../public/Reducer";
import {requestGet, requestPost} from '../public/Request';

@connect(
    state => state,
    {setColumns, setVisible, setRichText, changeTableParams, setReqUrl}
)
export default class User extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bordered: true,
            size: 'default',
            hasData: true,
            tableData: []
        };
        this.columns = [{
            title: 'id',
            dataIndex: 'id',
            sorter: true,
            key: 'id',
            width: 60,
        }, {
            title: '头像',
            dataIndex: 'avatar',
            key: 'avatar',
            width: 60,
            render: function (text, record, index) {
                return <a href={'javascript:'} target="view_window" onClick={this.showModal}><img src={text} style={{
                    width: 40,
                    height: 40
                }}/></a>
            }.bind(this)
        }, {
            title: '名字',
            dataIndex: 'name',
            key: 'name',
            width: 60,
        }, {
            title: '邮件',
            dataIndex: 'email',
            key: 'email',
            width: 60,
        }, {
            title: '性别',
            dataIndex: 'sex',
            key: 'sex',
            width: 60,
            render: function (text, record, index) {
                return <div>
                    {text == 1 ? "男" : "女"}
                </div>
            }.bind(this)
        },
            {
                title: '操作',
                dataIndex: 'edit',
                key: 'edit',
                width: 60,
                render: function (text, record, index) {
                    return <div><Link to={"user-edit/" + record.id}>编辑</Link>
                        <Popconfirm title="确定删除吗?" onConfirm={() => this.handleDelete(record.id)}>
                            <a href="javascript:" style={{paddingLeft: 30}}> 删除</a>
                        </Popconfirm></div>
                }.bind(this)
            }
        ];
    }

    handleDelete = async (key) => {
        const data = [...this.props.table.rows];
        if (window.localStorage.getItem('id') == key) {
            message.error('自己不能删除自己哦!');
        } else {
            let params = {id: key}
            let resultData = await requestPost('user/delete', params);
            if (resultData.success == true) {
                message.success('删除成功');
                this.props.changeTableParams({rows: data.filter(item => item.id !== key)});
            } else {
                message.error('删除失败');
            }

        }

    }


    showModal = (e) => {
        let imgElement = '<img src="' + e.target.src + '" width="600px"/>';
        this.props.setRichText(imgElement);
        this.props.setVisible(true)
    }


    async componentDidMount() {
        await this.props.setReqUrl("user/list");
        let {
            changeTableParams,
            table: {
                page,
                order
            }
        } = this.props;

        let params = {page, order}
        let data = await requestGet('user/list', params);
        if (data) {
            params.total = data.total;
            params.rows = data.data;
            console.log(111);
        } else {
            message.error("请求数据失败")
        }
        params.loading = false;
        changeTableParams && changeTableParams(params);

        const columns = this.columns;
        this.props.setColumns(columns);

    }


    render() {
        return (
            <div>
                <h3>用户信息</h3>
                <Divider></Divider>
                <Button type="primary" htmlType="submit" className="login-form-button loginBtn"
                        style={{marginBottom: 16}}>
                    <Link to={"user-add"}>添加用户</Link>
                </Button>
                <ModalCustom width={650} title={'图片预览'}>

                </ModalCustom>
                <TableCustom >
                </TableCustom>

            </div>
        );
    }
}
