import {
    Form,
    Input,
    Button,
    Radio,
    message,
    Divider, Breadcrumb
} from 'antd';
import React from 'react';
const FormItem = Form.Item;
import { Link} from 'react-router-dom';
import UploadCustom from "../public/UploadCustom";
import {requestGet, requestPost} from "../public/Request";


class UserInputForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            loading: false,
            imgUrl:''
        };
        this.handleSubmit=this.handleSubmit.bind(this);
        this.handleConfirmBlur=this.handleConfirmBlur.bind(this);
        this.compareToFirstPassword=this.compareToFirstPassword.bind(this);
        this.validateToNextPassword=this.validateToNextPassword.bind(this);
        this.handleChange=this.handleChange.bind(this);
    }

    handleSubmit (e) {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll(async(err, values) => {
            if (!err) {
                let id= this.props.match.params.id;
                let params={
                    name: values.nickname,
                    phone:values.phone,
                    email:values.email,
                    avatar: values.avatar,
                    password: values.password,
                    sex: values.sex
                };
                if(!id){
                    let resultData= await requestPost('user/add',params);

                    if(resultData.success==true){
                        message.success('添加成功', 1, function () {
                            this.props.history.push('/user');
                        }.bind(this));
                    }
                    else {
                        if (resultData.msg=='repeat'){
                            message.error('邮箱或者电话重复');
                        }
                    }
                }
                else{
                    params={
                        ...params,id
                    };
                    let resultData= await requestPost('user/update',params);

                    if(resultData.success==true){
                        message.success('添加成功', 1, function () {
                            this.props.history.push('/user');
                        }.bind(this));
                    }
                    else {
                        if (resultData.msg == 'repeat') {
                            message.error('邮箱或者电话重复');
                        }
                    }
                }


            }
        });
    }

    handleConfirmBlur (e){
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }

    handleChange =(imageUrl)=> {
        this.props.form.setFieldsValue({avatar:imageUrl});

    }

    compareToFirstPassword (rule, value, callback)  {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }
    }

    validateToNextPassword (rule, value, callback) {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], { force: true });
        }
        callback();
    }

    async componentWillMount() {
        let id = this.props.match.params.id;
        let params = {id:id};
        let resultData = await requestGet('user/detail', params);
        if (resultData) {
            let data=resultData;
            this.props.form.setFieldsValue({
                nickname: data.name,
                phone: data.phone,
                email: data.email,
                sex: data.sex,
                password: data.password,
                confirm: data.password,
                avatar:data.avatar
            })
        }
    }


    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 3 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 10 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 6,
                },
            },
        };


        return (
            <Form onSubmit={this.handleSubmit}>
                <Breadcrumb separator=">">
                    <Breadcrumb.Item> <Link to={'/'}>首页</Link></Breadcrumb.Item>
                    <Breadcrumb.Item> <Link to={'/user'}>用户列表</Link></Breadcrumb.Item>
                    <Breadcrumb.Item>用户添加</Breadcrumb.Item>
                </Breadcrumb>
                <br/>
                <h3>用户添加</h3>
                <Divider >

                </Divider>
                <FormItem
                    {...formItemLayout}
                    label="头像"
                >
                    {getFieldDecorator('avatar')(
                        <UploadCustom uploadSuccess={this.handleChange} imageUrl={this.props.form.getFieldValue('avatar')}
                                      style={{width:150,height:150}}>

                        </UploadCustom>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label='名字'
                >
                    {getFieldDecorator('nickname', {
                        rules: [{ required: true, message: '请输入您的名字！', whitespace: true }],
                    })(
                        <Input />
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="电话号码"
                >
                    {getFieldDecorator('phone', {
                        rules: [{ required: true,  message:'请输入您的电话!'}],
                            getValueFromEvent: (event) => {
                                return event.target.value.replace(/\D/g,'')
                            },
                    }
                    )(
                        <Input style={{ width: '100%' }} maxLength={11} />
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="邮箱"
                >
                    {getFieldDecorator('email', {
                        rules: [{
                            type: 'email', message: '邮箱格式不符合要求!',
                        }, {
                            required: true, message: '请输入邮箱!',
                        }],
                    })(
                        <Input />
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="性别"
                >
                    {getFieldDecorator('sex',   {initialValue: '1'})(
                        <Radio.Group  buttonStyle="solid">
                            <Radio.Button value="1" >男</Radio.Button>
                            <Radio.Button value="2">女</Radio.Button>
                        </Radio.Group>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="密码"
                >
                    {getFieldDecorator('password', {
                        rules: [{
                            required: true, message: '请输入您的密码!',
                        }, {
                            validator: this.validateToNextPassword,
                        }],
                    })(
                        <Input type="password" />
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="密码确认"
                >
                    {getFieldDecorator('confirm', {
                        rules: [{
                            required: true, message: '请再次输入您的密码!',
                        }, {
                            validator: this.compareToFirstPassword,
                        }],
                    })(
                        <Input type="password" onBlur={this.handleConfirmBlur} />
                    )}
                </FormItem>
                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">Register</Button>
                </FormItem>
            </Form>
        );
    }
}

const UserInput = Form.create()(UserInputForm);
export default UserInput;