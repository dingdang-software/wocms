import React from 'react';
import {Link} from 'react-router-dom';

import {
    Divider, message, Button, Popconfirm
} from 'antd';
import ModalCustom from "../public/ModalCustom";
import {TableCustom} from "../public/TableCustom";
import connect from "react-redux/es/connect/connect";
import {setColumns, setVisible, setRichText, changeTableParams, setReqUrl} from "../public/Reducer";
import {requestGet, requestPost} from '../public/Request';
import moment from "moment";

@connect(
    state => state,
    {setColumns, setVisible, setRichText, changeTableParams, setReqUrl}
)
export default class Recruit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bordered: true,
            size: 'default',
            hasData: true,
            tableData: [],
            visible: false
        };
        this.columns = [{
            title: 'id',
            dataIndex: 'id',
            sorter: true,
            key: 'id',
            width: 10,
        }, {
            title: '招聘职业',
            dataIndex: 'job',
            key: 'job',
            width: 60,

        }, {
            title: '招聘类别',
            dataIndex: 'recruit_type',
            key: 'recruit_type',
            width: 60,
        }, {
            title: '工作性质',
            dataIndex: 'job_nature',
            key: 'job_nature',
            width: 60,
        }, {
            title: '薪资范围',
            dataIndex: 'salary',
            key: 'salary',
            width: 60,
        }, {
            title: '招聘人数',
            dataIndex: 'num',
            key: 'num',
            width: 60,
        }, {
            title: '工作地点',
            dataIndex: 'work_at',
            key: 'work_at',
            width: 60,
        }, {
            title: '招聘要求',
            dataIndex: 'skill_requirement',
            key: 'skill_requirement',
            width: 60,
            render: function (text, record, index) {
                return <div>
                    <a type="primary" href={'javascript:'} onClick={this.showModal} className={text}>
                        查看要求
                    </a>
                </div>
            }.bind(this)

        }, {
            title: '发布时间',
            dataIndex: 'published_at',
            key: 'published_at',
            width: 60,
            render: text => {
                return moment(text).format("YYYY-MM-DD");
            }
        }, {
            title: '截止时间',
            dataIndex: 'expired_at',
            key: 'expired_at',
            width: 60,
            render: text => {
                return moment(text).format("YYYY-MM-DD");
            }
        }, {
            title: '操作',
            dataIndex: 'edit',
            key: 'edit',
            width: 60,
            render: function (text, record, index) {
                return <div><Link to={"recruit-edit/" + record.id}>编辑</Link>
                    <Popconfirm title="确定删除吗?" onConfirm={() => this.handleDelete(record.id)}>
                        <a href="javascript:" style={{paddingLeft: 30}}> 删除</a>
                    </Popconfirm>
                </div>
            }.bind(this)
        }
        ];
    }


    handleDelete = async (key) => {
        const data = [...this.props.table.rows];
        let params = {id: key}
        let resultData = await requestPost('recruit/delete', params);
        if (resultData.success == true) {
            message.success('删除成功');
            this.props.changeTableParams({rows: data.filter(item => item.id !== key)});
        } else {
            message.error('删除失败');
        }

    }

    showModal = (e) => {
        this.props.setRichText(e.target.className);
        this.props.setVisible(true)
    }


    async componentDidMount() {

        await this.props.setReqUrl("recruit/list");
        let {
            changeTableParams,
            table: {
                page,
                order
            }
        } = this.props;

        let params = {page, order}
        let data = await requestGet('recruit/list', params);
        if (data) {
            params.total = data.total;
            params.rows = data.data;
        } else {
            message.error("请求数据失败")
        }

        params.loading = false;
        changeTableParams && changeTableParams(params);
        const columns = this.columns;
        this.props.setColumns(columns);
    }


    render() {

        return (
            <div>
                <h3>招聘信息</h3>
                <Divider></Divider>
                <Button type="primary" htmlType="submit" className="login-form-button loginBtn"
                        style={{marginBottom: 16}}>
                    <Link to={"recruit-add"}>添加招聘信息</Link>
                </Button>

                <ModalCustom width={1000} title={'富文本预览'}>
                </ModalCustom>


                <TableCustom >
                </TableCustom>


            </div>
        );
    }
}
