import {Form, Input, Button, message, InputNumber, DatePicker, Cascader, Select, Divider, Breadcrumb} from 'antd';
import React from 'react';

const FormItem = Form.Item;

import moment from 'moment';

const RangePicker = DatePicker.RangePicker;
import {Link} from "react-router-dom";
import CKEditorCustom from "../public/CKEditorCustom";
import {requestGet, requestPost} from "../public/Request";

const {Option} = Select;

class RecruitInputForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            loading: false,
            imgUrl: '',
            content: '',
            typeList: []
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }


    onChange(content) {
        this.props.form.setFieldsValue({skill_requirement: content})

    }


    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll(async (err, values) => {
            if (!err) {

                let typeList = this.state.typeList;
                let type = '';
                for (let i = 0; i < typeList.length; i++) {
                    if (typeList[i].id == values.recruit_type_id) {
                        type = typeList[i].o_type
                    }
                }

                let params = {
                    job: values.job,
                    recruit_type_id: values.recruit_type_id,
                    recruit_type: type,
                    job_nature: values.job_nature,
                    salary: values.salary,
                    num: values.num,
                    work_at: values.work_at,
                    skill_requirement: values.skill_requirement,
                    published_at: values.date[0].format('YYYY-MM-DD'),
                    expired_at: values.date[1].format('YYYY-MM-DD')
                };

                let id = this.props.match.params.id;
                if (!id) {
                    let resultData = await requestPost('recruit/add', params);

                    if (resultData.success == true) {
                        message.success('添加成功', 1, function () {
                            this.props.history.push('/recruit');
                        }.bind(this))
                    } else {
                        message.error('添加失败');
                    }
                } else {
                    params={...params,id};
                    let resultData = await requestPost('recruit/update', params);
                    if (resultData.success == true) {
                        message.success('更新成功', 1, function () {
                            this.props.history.push('/recruit');
                        }.bind(this));
                    } else {
                        message.error('更新失败');
                    }
                }


            }
        });
    }


    async componentDidMount() {


        let params = {};
        let resultData = await requestGet('recruit-types', params);
        if (resultData) {
            let data = resultData;
            const listItems = data.map((item, index) =>
                <Option value={item.id} key={index}>{item.o_type}</Option>
            );


            await this.setState({typeListItem: listItems, typeList: data});
        }


        let id = this.props.match.params.id;
        if (id) {
            let recruitParams = {id: id};
            let recruitData = await requestGet('recruit/detail', recruitParams);
            if (recruitData) {
                let data = recruitData;
                this.props.form.setFieldsValue({
                    job: data.job,
                    recruit_type_id: data.recruit_type_id,
                    job_nature: data.job_nature,
                    salary: data.salary,
                    num: data.num,
                    work_at: data.work_at,
                    skill_requirement: data.skill_requirement,
                    date: [moment(data.published_at, "YYYY-MM-DD"), moment(data.expired_at, "YYYY-MM-DD")]
                });
            }
        }

    }


    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 10},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 6,
                },
            },
        };

        return (
            <Form onSubmit={this.handleSubmit}>
                <Breadcrumb separator=">">
                    <Breadcrumb.Item> <Link to={'/'}>首页</Link></Breadcrumb.Item>
                    <Breadcrumb.Item> <Link to={'/recruit'}>招聘列表</Link></Breadcrumb.Item>
                    <Breadcrumb.Item>招聘添加</Breadcrumb.Item>
                </Breadcrumb>
                <br/>
                <h3>招聘信息添加</h3>
                <Divider></Divider>

                <FormItem
                    {...formItemLayout}
                    label="职位"
                >
                    {getFieldDecorator('job', {
                        rules: [{
                            required: true, message: '请输入职位!',
                        }]
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="招聘类型"
                >
                    {getFieldDecorator('recruit_type_id', {
                        rules: [{
                            required: true, message: '请选择招聘类型!',
                        }]
                    })(
                        <Select placeholder="请选择招聘类型">
                            {this.state.typeListItem}
                        </Select>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="工作类型"
                >
                    {getFieldDecorator('job_nature', {
                        initialValue: "全职",
                        rules: [{
                            required: true, message: '请输入工作类型！'
                        }]
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="薪资范围"
                >
                    {getFieldDecorator('salary', {
                        initialValue: "面议",
                        rules: [{
                            required: true, message: '请输入薪资范围！'
                        }]
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="招聘人数"
                >
                    {getFieldDecorator('num', {
                        initialValue: "1",
                        rules: [{
                            required: true, message: '请输入招聘人数！'
                        }]
                    })(
                        <InputNumber min={1}/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="工作地点"
                >
                    {getFieldDecorator('work_at', {
                        rules: [{
                            required: true, message: '请输入工作地点！'
                        }]
                    })(
                        <Input placeholder={'请输入工作地点'}/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="发布、截止时间"
                >
                    {getFieldDecorator('date', {
                        rules: [{
                            required: true, message: '请输入发布、截止日期！'
                        }]
                    })(
                        <RangePicker
                            ranges={{
                                Today: [moment(), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')]
                            }}
                            placeholder={['发布日期', '截止日期']}
                        />
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="招聘要求"
                >
                    {getFieldDecorator('skill_requirement')(
                        <CKEditorCustom onChange={this.onChange}
                                        content={this.props.form.getFieldValue('skill_requirement')}>

                        </CKEditorCustom>
                    )}
                </FormItem>


                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">添加</Button>
                </FormItem>
            </Form>
        );
    }
}

const RecruitInput = Form.create()(RecruitInputForm);
export default RecruitInput;