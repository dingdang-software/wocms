import {Form, Input, Button, message, InputNumber, DatePicker, Cascader, Select, Divider, Breadcrumb} from 'antd';
import React from 'react';
const FormItem = Form.Item;
import {Link} from "react-router-dom";
import CKEditorCustom from "../public/CKEditorCustom";
import {requestGet, requestPost} from "../public/Request";
const { Option } = Select;



class Footer1InputForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            loading: false,
            imgUrl:'',
            content:'',
            typeList:[]
        };
        this.handleSubmit=this.handleSubmit.bind(this);
        this.onChange=this.onChange.bind(this);
    }



    onChange(content){
        this.props.form.setFieldsValue({link_txt:content})

    }

    async componentDidMount() {

        let id=this.props.match.params.id;
        console.log(id);
        let params={
            id:id
        }

        let resultData= await requestGet('footer1/detail',params);

        if (resultData) {
            let data=resultData;
            this.props.form.setFieldsValue({
                display_position: data.display_position,
                link_url:data.link_url,
                link_txt:data.link_txt
            });

        }
    }

    handleSubmit (e) {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll(async (err, values) => {
            if (!err) {
                let id=this.props.match.params.id;
                if(id){
                    let params={
                        id:id,
                        display_position: values.display_position,
                        link_url:values.link_url,
                        link_txt:values.link_txt,
                    };
                    let resultData= await requestPost('footer1/update',params);


                    if (resultData.success == true) {
                        message.success('修改成功',1, function () {
                            console.log(this.props.history.push('/footer1'));
                        }.bind(this));

                    }
                    else {
                        message.error('修改失败');
                    }
                }
                else {
                    let params={
                        display_position: values.display_position,
                        link_url:values.link_url,
                        link_txt:values.link_txt,
                    };
                    let resultData= await requestPost('footer1/add',params);


                    if (resultData.success == true) {
                        message.success('添加成功',1, function () {
                            // this.props.history.push('/article');
                            this.props.history.push('/footer1')
                        }.bind(this));

                    }
                    else {
                        message.error('添加失败');
                    }
                }

            }
        });
    }






    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 3 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 20 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 10,
                },
            },
        };

        return (
            <Form onSubmit={this.handleSubmit}>
                <Breadcrumb separator=">">
                    <Breadcrumb.Item> <Link to={'/'}>首页</Link></Breadcrumb.Item>
                    <Breadcrumb.Item> <Link to={'/footer1'}>页脚1列表</Link></Breadcrumb.Item>
                    <Breadcrumb.Item>页脚1添加</Breadcrumb.Item>
                </Breadcrumb>
                <br/>
                <h3>页脚1添加</h3>
                <Divider ></Divider>

                <FormItem
                    {...formItemLayout}
                    label="展示位置"
                >
                    {getFieldDecorator('display_position',{
                        rules: [ {
                            required: true, message: '请选择展示位置!',
                        }]})(
                        <Select placeholder="请选择展示位置">
                            <Option value={'left'} key={1}>左边</Option>
                            <Option value={'center'} key={2}>居中</Option>
                            <Option value={'right'} key={3}>右边</Option>
                        </Select>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="导航链接"
                >
                    {getFieldDecorator('link_url')(
                        <Input   placeholder={'请输入导航链接'}/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="富文本内容"
                >
                    {getFieldDecorator('link_txt')(
                        <CKEditorCustom  onChange={this.onChange} content={this.props.form.getFieldValue('link_txt')}>

                        </CKEditorCustom>
                    )}
                </FormItem>


                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">添加</Button>
                </FormItem>
            </Form>
        );
    }
}

const Footer1Input = Form.create()(Footer1InputForm);
export default Footer1Input;