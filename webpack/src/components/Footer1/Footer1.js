import React from 'react';
import {Link} from 'react-router-dom';

import {Divider, message, Button, Popconfirm} from 'antd';
import ModalCustom from "../public/ModalCustom";
import {TableCustom} from "../public/TableCustom";
import connect from "react-redux/es/connect/connect";
import {setColumns, setVisible, setRichText, changeTableParams, setReqUrl} from "../public/Reducer";
import {requestGet, requestPost} from '../public/Request';

@connect(
    state => state,
    {setColumns, setVisible, setRichText, changeTableParams, setReqUrl}
)

export default class Footer1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bordered: true,
            size: 'default',
            hasData: true,
            tableData: [],
            visible: false
        };
        this.columns = [{
            title: 'id',
            dataIndex: 'id',
            sorter: true,
            key: 'id',
            width: '10%',
        }, {
            title: '展示位置',
            dataIndex: 'display_position',
            key: 'display_position',
            width: 60,

        }, {
            title: '富文本内容',
            dataIndex: 'link_txt',
            key: 'link_txt',
            width: 60,
            render: function (text, record, index) {
                return <div>
                    <a type="primary" href={'javascript:'} onClick={this.showModal} className={text}>
                        查看内容
                    </a>
                </div>
            }.bind(this)
        }, {
            title: '导航链接',
            dataIndex: 'link_url',
            key: 'link_url',
            width: 60,
        }, {
            title: '操作',
            dataIndex: 'edit',
            key: 'edit',
            width: 60,
            render: function (text, record, index) {
                return <div><Link to={"footer1-edit/" + record.id}>编辑</Link>
                    <Popconfirm title="确定删除吗?" onConfirm={() => this.handleDelete(record.id)}>
                        <a href="javascript:" style={{paddingLeft: 30}}> 删除</a>
                    </Popconfirm>
                </div>
            }.bind(this)
        }
        ]
    }


    handleDelete = async (key) => {
        const data = [...this.props.table.rows];
        let params = {id: key}
        let resultData = await requestPost('footer1/delete', params);
        console.log(resultData);
        if (resultData.success) {
            message.success('删除成功');
            this.props.changeTableParams({rows: data.filter(item => item.id !== key)});
        } else {
            message.error('删除失败');

        }

    }


    showModal=(e)=> {
        this.props.setRichText(e.target.className);
        this.props.setVisible(true)
    }


    async componentDidMount() {
        await this.props.setReqUrl("footer1/list");
        let {
            changeTableParams,
            table: {
                page,
                order
            }
        } = this.props;

        let params = {page, order}
        let data = await requestGet('footer1/list', params);
        if (data) {
            params.total = data.total;
            params.rows = data.data;
        } else {
            message.error("请求数据失败")
        }

        params.loading = false;
        changeTableParams && changeTableParams(params);

        const columns = this.columns;
        this.props.setColumns(columns);
    }


    render() {
        return (
            <div>
                <h3>页脚1信息</h3>
                <Divider></Divider>
                <Button type="primary" htmlType="submit" className="login-form-button loginBtn"
                        style={{marginBottom: 16}}>
                    <Link to={"footer1-add"}>添加页脚1信息</Link>
                </Button>

                <ModalCustom width={1000} title={'富文本预览'}>
                </ModalCustom>

                <TableCustom >
                </TableCustom>
            </div>
        );
    }
}
