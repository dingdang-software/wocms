import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Route} from 'react-router';
import {HashRouter, Router, Link, Switch} from 'react-router-dom';

import Navigation from './Navigation'
import Login from './Login/Login'
import Home from './Home/Home'
import ResetPass from "./ResetPass/ResetPass";
require("../styles/App.scss");
import {
    createBrowserHistory,
} from 'history'


import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';

import {rootReducer} from './public/Reducer';
const store = createStore(rootReducer,compose(
    applyMiddleware(thunk),//创建异步Redux
    //引入Chrome中的Redux开发者工具
    //修改代码判断系统是否存在函数，若没有则返回空
    window.devToolsExtension ? window.devToolsExtension() : f => f
));



export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {

        return (
            <Provider store={store}>
                <Router history={createBrowserHistory()}>
                    <Switch>
                        <Route exact path="/" component={Navigation} />
                        <Route  path="/login" component={Login} ></Route>
                        <Route path="/resetpass" component={ResetPass}></Route>

                    </Switch>

                </Router>
            </Provider>
        );
    }
}
