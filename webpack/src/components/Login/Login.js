
import React, { Component } from 'react';
import axios  from 'axios';
import {Form, Icon, Input, Button,Checkbox} from 'antd';
import {requestGet} from "../public/Request";
const FormItem = Form.Item;
import {serverUrl} from '../public/config'
class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            account:'',
            password:'',
            securityCode:''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e){
        e.preventDefault();
        let history=this.props.history;
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                axios.post(serverUrl+"login",{
                        account: values.username,
                        password: values.password
                    }
                ).then(function (response) {
                    console.log(response);
                    if(response.data.success==true){
                        let userinfo=response.data.data[0];
                        window.localStorage.setItem("id",userinfo.id);
                        window.localStorage.setItem("avatar",userinfo.avatar);
                        window.localStorage.setItem("name",userinfo.name);
                        window.localStorage.setItem("email",userinfo.email);
                        window.localStorage.setItem("phone",userinfo.phone);
                        window.localStorage.setItem("sess_id",response.data.session_id);
                        history.push('/');
                    }
                }).catch(function (error) {
                    console.log(error);
                })
            }
        });
    }

    async componentDidMount() {
        // To disabled submit button at the beginning.
        this.props.form.validateFields();
        let params={};
        let data= await requestGet('company/logo',params);
        if(data) {
            this.setState({logo: data.logo, corpName:data.c_name});

        }

    }

    render() {
        const {
            getFieldDecorator
        } = this.props.form;

        return (
            <div className="loginContainer">
                <div className="login">

                    <img  style={{width:100,marginBottom:10}} src={this.state.logo} alt=""/>
                    <div className="corpName" style={{marginBottom:10}}>{this.state.corpName}</div>

                    <Form onSubmit={this.handleSubmit} style={{width: '300px'}}>
                        <FormItem>
                            {getFieldDecorator('username', {
                                rules: [{ required: true, message: '请输入用户名!' }],
                            })(
                                <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="用户名 (admin)" />
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: '请输入密码!' }],
                            })(
                                <Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="密码 (admin)" />
                            )}
                        </FormItem>
                        <FormItem style={{marginBottom:'0'}}>
                            {getFieldDecorator('remember', {
                                valuePropName: 'checked',
                                initialValue: true,
                            })(
                                <Checkbox>记住我</Checkbox>
                            )}
                            <a className="login-form-forgot" href="/resetpass" style={{float:'right'}}>忘记密码?</a>
                            <Button type="primary" htmlType="submit" className="login-form-button loginBtn" style={{width: '100%'}}>
                                登录
                            </Button>
                            <div>&nbsp;</div>
                        </FormItem>
                    </Form>
                </div>
            </div>
        );
    }
}
const Login = Form.create()(LoginForm);
export default Login;