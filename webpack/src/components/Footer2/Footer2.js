import {
    message, Button, Divider
} from 'antd';
import React from 'react';
import {TableEditCustom} from '../public/TableEditCustom'
import TableOperation from '../public/TableOperation'
import connect from "react-redux/es/connect/connect";
import {setColumns, setEditingKey, setSelectArr, changeTableParams, setReqUrl} from "../public/Reducer";
import {requestGet, requestPost} from '../public/Request';

@connect(
    //需要的属性
    state => state,
    //需要的方法，自动dispatch
    {setColumns, setEditingKey, setSelectArr, changeTableParams, setReqUrl}
)

class Footer2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {data: [], editingKey: '', addRows: []};
        this.columns = [
            {
                title: 'id',
                dataIndex: 'id',
                width: '10%',
                sorter: true,
                editable: false,
            },
            {
                title: '分组栏目',
                dataIndex: 'col',
                width: '20%',
                editable: true,
                render: (text, record) => {
                    let obj = text ? JSON.parse(text) : null;
                    return (
                        <div>{obj ? obj.col_title : ''}</div>
                    )
                }
            },
            {
                title: '链接文字',
                dataIndex: 'link_txt',
                width: '20%',
                editable: true,
            },
            {
                title: '链接',
                dataIndex: 'link_url',
                width: '20%',
                editable: true,
            },
            {
                title: '权重',
                dataIndex: 'weight',
                width: '10%',
                editable: true,
            },
            {
                title: '操作',
                dataIndex: 'operation',
                render: (text, record) => {
                    return (
                        <TableOperation record={record} save={this.save} handleDelete={this.handleDelete}>

                        </TableOperation>
                    )
                }
            },
        ];

    }

    isEditing = record => record.id === this.props.table.editingKey;


    save = (form, key) => {
        form.validateFields(async (error, row) => {
            if (error) {
                return;
            }
            const newData = [...this.props.table.rows];
            let isAddRow = false;

            const index = newData.findIndex(item => key === item.id);

            if (index > -1) {
                const item = newData[index];

                for (let i in this.state.addRows) {
                    if (this.state.addRows[i] == key) {
                        isAddRow = true;
                    }
                }
                let obj = JSON.parse(row.col);
                let postUrl = isAddRow ? 'footer2/add' : 'footer2/update';

                let params = {
                    id: key,
                    col_id: obj.col_id,
                    col_title: obj.col_title,
                    link_txt: row.link_txt,
                    link_url: row.link_url,
                    weight: row.weight,
                };
                let resultData = await requestPost(postUrl, params);
                if (resultData.success == true) {
                    message.success('修改成功');
                    row.id = resultData.data;
                    row.user_id = window.localStorage.getItem('id'),
                        newData.splice(index, 1, {
                            ...item,
                            ...row,
                        });
                    this.props.setEditingKey('');
                    this.props.changeTableParams({rows: newData});
                } else {
                    message.error('修改失败');
                }


            } else {
                newData.push(row);
                this.props.setEditingKey('');
                this.props.changeTableParams({rows: newData});
            }
        });
    }


    handleAdd = () => {
        const data = this.props.table.rows;
        const newData = {
            id: Math.random(),
            col: '',
            link_txt: '输入链接文字',
            link_url: '/',
            weight: '1',
        };
        const addRows = this.state.addRows;
        addRows.push(newData.id)
        this.setState({addRows});
        this.props.changeTableParams({rows: [...data, newData]});
    }

    handleDelete = async (key) => {
        const data = [...this.props.table.rows];
        let params = {id: key}
        let resultData = await requestPost('footer2/delete', params);
        console.log(resultData);
        if (resultData.success) {
            message.success('删除成功');
            this.props.changeTableParams({rows: data.filter(item => item.id !== key)});
        } else {
            message.error('删除失败');
            for (let i in this.state.addRows) {
                if (this.state.addRows[i] == key) {
                    this.props.changeTableParams({rows: data.filter(item => item.id !== key)});
                }
            }
        }
    }


    async componentDidMount() {

        await this.props.setReqUrl("footer2/list");
        let {
            changeTableParams,
            table: {
                page,
                order
            }
        } = this.props;

        let params = {page, order}
        let data = await requestGet('footer2/list', params);
        if (data) {
            params.total = data.total;
            params.rows = data.data;
        } else {
            message.error("请求数据失败")
        }

        params.loading = false;
        changeTableParams && changeTableParams(params);

        let colParams = {}
        let colData = await requestGet('footer2-col', colParams);
        if (colData) {
            this.props.setSelectArr(colData)
        }


        const columns = this.columns.map((col) => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    inputtype: col.dataIndex === 'col' ? 'select' : (col.dataIndex === 'weight' ? 'number' : 'text'),
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                    selectArr: this.props.table.selectArr
                }),
            };

        });
        this.props.setColumns(columns);

    }

    render() {

        return (
            <div>
                <h3>页脚2信息</h3>
                <Divider>

                </Divider>
                <Button onClick={this.handleAdd} type="primary" style={{marginBottom: 16}}>
                    添加一个页脚2数据
                </Button>

                <TableEditCustom >
                </TableEditCustom>
            </div>

        );
    }
}

export default Footer2;