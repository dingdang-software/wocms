import {setColumns, setTableData, setVisible, setRichText, setOrder, setPage} from "./Reducer";
import axios from "axios";
import {message} from "antd";
import {serverUrl} from './config'
export async function requestGet(url, params) {
    let responseData = null;
    await axios.get(serverUrl+"" + url, {
        headers: {
            sess_id: window.localStorage.getItem('sess_id')
        },
        params: {
            uid: window.localStorage.getItem('id'),
            ...params
        }
    }).then(function (response) {
        console.log(response.data);
        if (response.data.success == true) {
            responseData = response.data.data;
        } else {
            if (response.data.msg == 'invalid') {
                message.error("登录失效，请重新登录,两秒后跳转", 2, function () {
                    window.location.href = '/login';
                });


            }
        }
    }.bind(this)).catch(function (error) {
        console.log(error);
    })

    return responseData;
}

export async function requestPost(url, data) {
    let responseData = null;
    await axios.post(serverUrl + url, {
            uid: window.localStorage.getItem('id'),
            ...data
        }
        , {
            headers: {
                sess_id: window.localStorage.getItem('sess_id')
            }
        }).then(function (response) {
        console.log(response.data);
        if (response.data.success == true) {
            responseData = response.data;
        } else {
            if (response.data.msg == 'invalid') {
                message.error("登录失效，请重新登录,两秒后跳转", 2, function () {
                    window.location.href = '/login';
                });

            }
            responseData = response.data;
        }
    }.bind(this)).catch(function (error) {
        console.log(error);
    })

    return responseData;
}