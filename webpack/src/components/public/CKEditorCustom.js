import {Icon, message, Modal, Upload} from 'antd';
import React, {Component} from 'react';
import DecoupledEditor  from '@ckeditor/ckeditor5-build-decoupled-document';
import CKEditor from "@ckeditor/ckeditor5-react";
import axios from "axios";
import {serverUrl} from './config'
class UploadAdapter {
    constructor(loader,url) {
        this.loader = loader;
        this.url = url;
    }
    upload() {
        return new Promise((resolve, reject) => {
            const data = new FormData()
            data.append('image', this.loader.file, 'name.jpg')
            axios.post(serverUrl+"image/upload",data
            ).then(function (response) {
                if(response.data.success==true){
                    resolve({
                        default: serverUrl+response.data.data.uri
                    });
                }
                else {
                    reject(data.msg);
                }
            }.bind(this)).catch(function (error) {
                console.log(error);
            })

        });
    }
    abort() {
    }
}

export default class CKEditorCustom extends React.Component  {
    constructor(props) {
        super(props);
        this.state={

        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.content){
            this.setState({content:nextProps.content})
        }
    }

    onChange=(evt,editor)=>{
        this.setState({content:editor.getData()})
        this.props.onChange(editor.getData());
    }

    render() {

        return <CKEditor
            editor={ DecoupledEditor }
            data={this.state.content?this.state.content:''}
            onInit={editor => {
                editor.plugins.get('FileRepository').createUploadAdapter = (loader)=>{
                return new UploadAdapter(loader);
                }
                editor.ui.view.editable.element.parentElement.insertBefore(
                    editor.ui.view.toolbar.element,
                    editor.ui.view.editable.element
                );
                console.log(editor);
            }
            }
            onChange={this.onChange}

        />
    }
}