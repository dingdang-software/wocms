import {Popconfirm, Upload} from 'antd';
import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {setColumns, setEditingKey, setSelectArr} from "./Reducer";
import {EditableContext} from './TableEditCustom'

@connect(
    //需要的属性
    state => state,
    //需要的方法，自动dispatch
    { setColumns,setEditingKey,setSelectArr}
)
export default class TableOperation extends React.Component  {
    constructor(props) {
        super(props);
        this.state={

        }
    }

    componentWillReceiveProps(nextProps) {

    }

    isEditing = record => record.id === this.props.table.editingKey;

    cancel = () => {
         this.props.setEditingKey('');
    };
    edit(key) {
        this.props.setEditingKey(key);
    }


    render() {
        const record=this.props.record;
        const editable = this.isEditing(record);
        return <div>
            {editable ? (
                <span>
                  <EditableContext.Consumer>
                    {form => (
                        <a
                            href="javascript:;"
                            onClick={() => this.props.save(form, record.id)}
                            style={{ marginRight: 8 }}
                        >
                            保存
                        </a>
                    )}
                  </EditableContext.Consumer>
                  <Popconfirm
                      title="确定取消吗?"
                      onConfirm={() => this.cancel(record.id)}
                      okText={"确定"}
                      cancelText={"取消"}
                  >
                    <a>取消</a>
                  </Popconfirm>
                </span>
            ) : (
                <a onClick={() => this.edit(record.id)}>编辑</a>
            )}
            <Popconfirm title="确定删除吗?" onConfirm={() => this.props.handleDelete(record.id)}>
                <a href="javascript:" style={{paddingLeft:30}}> 删除</a>
            </Popconfirm>
        </div>
    }
}