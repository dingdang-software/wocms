import {Icon, message, Modal, Upload} from 'antd';
import React, {Component} from 'react';
import {serverUrl} from './config'
function beforeUpload(file) {
    const isJPG = file.type === 'image/jpeg'||file.type === 'image/png';
    if (!isJPG) {
        message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
}

export default class UploadCustom extends React.Component  {
    constructor(props) {
        super(props);
        this.state={
            loading:false
        }
    }

    handleChange =(info)=> {
        if (info.file.status === 'uploading') {
            this.setState({ loading: true });
            return;
        }
        if (info.file.status === 'done') {

            if(info.file.response.success){
                let data=info.file.response.data;
                this.setState({imageUrl:serverUrl+data.uri,loading:true})
                this.props.uploadSuccess(serverUrl+data.uri);
            }
        }
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.imageUrl){
            this.setState({imageUrl:nextProps.imageUrl})
        }
    }


    render() {
        const uploadButton = (
            <div>
                <Icon type={this.state.loading ? 'loading' : 'plus'} />
                <div className="ant-upload-text">Upload</div>
            </div>
        );
        return <Upload
            name="image"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action={serverUrl+"image/upload"}
            beforeUpload={beforeUpload}
            onChange={this.handleChange}
        >
            {this.state.imageUrl ? <img src={this.state.imageUrl} alt="avatar" style={this.props.style?this.props.style:{width:400}} /> : uploadButton}
        </Upload>
    }
}