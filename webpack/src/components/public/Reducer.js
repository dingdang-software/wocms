import { combineReducers } from 'redux';

//
const initTableParams = {
    page : 1,
    loading : true,
    total : 0,
    sort : "id",
    order : "desc",
    limit : 10,
    rows : [],
    exp : null,
    tableData : null
};

//状态管理

function table(state = initTableParams, action) {
    state = Object.assign({},state,{editingKey:null});
    switch (action.type) {

        case 'setColumns':
            state = Object.assign({},state,{columns:action.payload});
            break;

        case 'setEditingKey':
            state = Object.assign({},state,{editingKey:action.payload});
            break;
        case 'setSelectArr':
            state = Object.assign({},state,{selectArr:action.payload});
            break;
        case 'setVisible':
            state = Object.assign({},state,{visible:action.payload});
            break;
        case 'setRichText':
            state = Object.assign({},state,{richText:action.payload});
            break;
        case 'setReqUrl':
            state = Object.assign({},state,{reqUrl:action.payload});
            break;
        case 'changeTableParams':
            state = Object.assign({}, state, action.payload);
            break;
        case 'initTable':
            state = initTableParams;
            break;

        default:
            //初始状态
            //state={order:state.order==null?'desc':state.order,tableData:null};
            break;
    }
    return state;

}




//action creator 创建action
function setColumns(e) {
    return {type:'setColumns',payload:e}
}
function setEditingKey(e) {
    return {type:'setEditingKey',payload:e}
}
function setSelectArr(e) {
    return {type:'setSelectArr',payload:e}
}

function setVisible(e) {
    return {type:'setVisible',payload:e}
}
function setRichText(e) {
    return {type:'setRichText',payload:e}
}
function setReqUrl(e) {
    return {type : 'setReqUrl', payload : e}
}
/*
*
*  更改initTableParams参数
*
*/
function changeTableParams(params){
    return {type : 'changeTableParams', payload : params}
}
function initTable(params) {
    return {type : 'initTable', payload : params}
}





const rootReducer = combineReducers({
    table
});

export {
    rootReducer,
    setColumns,
    setEditingKey,
    setSelectArr,
    setVisible,
    setRichText,
    changeTableParams,
    initTable,
    setReqUrl,
};
