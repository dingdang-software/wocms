import {Form, Table, Input, InputNumber, Select, Switch, Pagination} from 'antd';
import React, {Component} from 'react';
import axios from "axios";
import connect from "react-redux/es/connect/connect";
import {requestGet} from "./Request";

const Option = Select.Option;
const FormItem = Form.Item;
import {
    setColumns,
    setEditingKey,
    setSelectArr,
    changeTableParams
} from "../public/Reducer";


const EditableContext = React.createContext();
const EditableRow = ({form, index, ...props}) => (
    <EditableContext.Provider value={form}>
        <tr {...props} />
    </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);


class EditableCell extends React.Component {
    getInput = (selectArr) => {
        if (this.props.inputtype === 'number') {
            return <InputNumber/>;
        }
        if (this.props.inputtype === 'select') {
            return <Select
                showSearch
                style={{width: 200}}
                placeholder="Select a person"
                optionFilterProp="children"
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >{
                selectArr.map(item => (
                    <Option key={item.id} value={'{"col_id":"' + item.id + '","col_title":"' + item.col_title + '"}'}>
                        {item.col_title}
                    </Option>
                ))
            }

            </Select>;
        }
        if (this.props.inputtype === 'radio') {
            return <Switch checkedChildren="开" unCheckedChildren="关" defaultChecked/>;
        }
        return <Input/>;
    };

    render() {
        const {
            selectArr,
            editing,
            dataIndex,
            title,
            record,
            ...restProps
        } = this.props;
        return (
            <EditableContext.Consumer>
                {(form) => {
                    const {getFieldDecorator} = form;
                    return (
                        <td {...restProps}>
                            {editing ? (
                                <FormItem style={{margin: 0}}>
                                    {getFieldDecorator(dataIndex, {
                                        rules: [{
                                            required: true,
                                            message: `请输入${title}!`,
                                        }],
                                        initialValue: record[dataIndex],
                                    })(this.getInput(selectArr))}
                                </FormItem>
                            ) : restProps.children}
                        </td>
                    );
                }}
            </EditableContext.Consumer>
        );
    }
}

@connect(
    //需要的属性
    state => state,
    //需要的方法，自动dispatch
    { setColumns, setEditingKey, setSelectArr, changeTableParams}
)
class TableEditCustom extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            editingKey: '',
            addRows: [],
            page:null,
            limit:null,
            order:null,
            sortBy:null
        };
    }


    componentWillReceiveProps(nextProps) {
        //console.log(nextProps);
    }


    handleChange = async (pagination, filters, sorter) => {
        console.log(pagination, filters, sorter);
        let {reqUrl} = this.props.table;
        let order = null;
        let sortBy = null;
        let {page ,limit}= this.state;

        if (JSON.stringify(sorter) !== "{}") {
            order = sorter.order.substr(0, sorter.order.length - 3);
            sortBy = sorter.column.dataIndex
            this.setState({order,sortBy});
        }

        let params = {page, order, limit, sortBy}
        let data = await requestGet(reqUrl, params);
        let tableParams = {
            rows: data.data
        };
        this.props.changeTableParams(tableParams)
    }

    handlePageChange = async (page) => {
        let {reqUrl} = this.props.table;
        this.setState({page});
        let {order, limit, sortBy}=this.state;
        let params = {page, order, limit, sortBy}
        let data = await requestGet(reqUrl, params);
        let tableParams = {
            rows: data.data
        };
        this.props.changeTableParams(tableParams)
    }

    handleShowSizeChange = async (page, limit) => {
        let {reqUrl} = this.props.table;
        this.setState({page,limit});
        let {order, sortBy}=this.state;
        let params = {page, order, limit, sortBy}
        let data = await requestGet(reqUrl, params);
        let tableParams = {
            rows: data.data
        };
        this.props.changeTableParams(tableParams)
    }

    render() {
        const components = {
            body: {
                row: EditableFormRow,
                cell: EditableCell,
            },
        };

        let {
            table: {loading, rows, total, columns}
        } = this.props;

        return <div>
            <Table
                rowKey={record => record.id}
                components={components}
                bordered
                loading={loading}
                dataSource={rows ? rows : null}
                columns={columns ? columns : null}
                rowClassName="editable-row"
                onChange={this.handleChange}
                pagination={false}
            />
            <div style={{width: '100%', display: 'flex', justifyContent: 'flex-end', marginTop: 20}}>
                <Pagination total={total} showSizeChanger showQuickJumper onChange={this.handlePageChange}
                            onShowSizeChange={this.handleShowSizeChange}
                />
            </div>
        </div>
    }
}

export {
    TableEditCustom,
    EditableContext
}