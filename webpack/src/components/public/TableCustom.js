import {Table,} from 'antd';
import React from 'react';
import connect from "react-redux/es/connect/connect";
import {requestGet} from "./Request";

import {
    setColumns,
    setEditingKey,
    setSelectArr,
    changeTableParams
} from "../public/Reducer";


@connect(
    //需要的属性
    state => state,
    //需要的方法，自动dispatch
    {setColumns, setEditingKey, setSelectArr, changeTableParams}
)
class TableCustom extends React.Component {
    constructor(props) {
        super(props);
        this.state = {data: [], editingKey: '', addRows: []};
    }


    componentWillReceiveProps(nextProps) {
        //console.log(nextProps);
    }


    handleChange = async (pagination, filters, sorter) => {
        let {reqUrl} = this.props.table;
        let order = null;
        let sortBy = null;

        let page = pagination.current;
        let limit = pagination.pageSize;

        if (JSON.stringify(sorter) !== "{}") {
            order = sorter.order.substr(0, sorter.order.length - 3);
            sortBy = sorter.column.dataIndex
        }


        let params = {page, order, limit, sortBy}
        let data = await requestGet(reqUrl, params);
        let tableParams = {
            rows: data.data
        };
        this.props.changeTableParams(tableParams)
    }

    render() {


        let {
            table: {loading, rows, total, columns}
        } = this.props;

        return <Table
            rowKey={record => record.id}
            bordered
            loading={loading}
            dataSource={rows ? rows : null}
            columns={columns ? columns : null}
            rowClassName="editable-row"
            pagination={
                {
                    total: total,
                    showSizeChanger: total >= 10,
                    showQuickJumper: true
                }
            }
            onChange={this.handleChange}
        />
    }
}

export {
    TableCustom
}