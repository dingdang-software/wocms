import {Modal} from  'antd';
import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import { setRichText, setVisible} from "./Reducer";
@connect(
    state => state,
    {  setVisible,setRichText}
)
export default class ModalCustom extends React.Component  {
    constructor(props) {
        super(props);
        this.state={
            visible: false,
            width:1000
        }
    }

    handleOk=(e)=> {
        this.props.setVisible(false)
    }

    handleCancel=(e)=> {
        this.props.setVisible(false)
    }



    componentWillReceiveProps(nextProps) {


    }

    render() {
        return <Modal
            title={this.props.title}
            visible={this.props.table.visible}
            width={this.props.width}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            cancelButtonProps={{className: "dis"}}
        >
            <div dangerouslySetInnerHTML={{__html: this.props.table.richText}} style={{width: '100%'}}
                 className={"modalContent"}/>
        </Modal>
    }
}