import {
    Popconfirm, message, Divider
} from 'antd';
import React from 'react';
import {TableCustom} from "../public/TableCustom";
import connect from "react-redux/es/connect/connect";
import {
    setColumns,
    setEditingKey,
    setSelectArr,
    changeTableParams,
    setReqUrl
} from "../public/Reducer";
import {requestGet, requestPost} from '../public/Request';

@connect(
    //需要的属性
    state => state,
    //需要的方法，自动dispatch
    {setColumns, setEditingKey, setSelectArr, changeTableParams, setReqUrl}
)

class Upload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {data: [], editingKey: '', addRows: []};
        this.columns = [
            {
                title: 'id',
                dataIndex: 'id',
                sorter: true,
                width: '5%',
                editable: false,
            },
            {
                title: '文件名',
                dataIndex: 'name',
                width: '30%',
                editable: true,
            },
            {
                title: '文件路径',
                dataIndex: 'uri',
                width: '30%',
                editable: true,
            },
            {
                title: '文件类型',
                dataIndex: 'type',
                width: '10%',
                editable: true,
            },
            {
                title: '大小',
                dataIndex: 'size',
                width: '10%',
                editable: true,
            },
            {
                title: '操作',
                dataIndex: 'operation',
                render: (text, record) => {
                    const editable = this.isEditing(record);
                    return (
                        <div>
                            <Popconfirm title="确定删除吗?" onConfirm={() => this.handleDelete(record.id)}>
                                <a href="javascript:" style={{paddingLeft: 30}}>删除</a>
                            </Popconfirm>
                        </div>
                    );
                },
            },
        ];
    }

    isEditing = record => record.id === this.props.table.editingKey;

    setTableData = (tableData) => {
        const data = [];
        for (let i = 0; i < tableData.length; i++) {
            data.push({
                key: tableData[i].id,
                id: tableData[i].id,
                name: tableData[i].name,
                uri: tableData[i].uri,
                type: tableData[i].type,
                size: tableData[i].size,
            });
        }
        console.log(data);
        this.props.setTableData(data);
    }


    handleDelete = async (key) => {
        console.log(key);
        const data = [...this.props.table.rows];
        let params = {id: key}
        let resultData = await requestPost('upload/delete', params);

        if (resultData.success) {
            message.success('删除成功');
            this.props.changeTableParams({rows: data.filter(item => item.id !== key)});
        } else {
            message.error('删除失败');
        }

    }


    async componentDidMount() {
        await this.props.setReqUrl("upload/list");
        let {
            changeTableParams,
            table: {
                page,
                order
            }
        } = this.props;


        let params = {page: page, order: order};


        let data = await requestGet('upload/list', params);
        if (data) {
            params.total = data.total;
            params.rows = data.data;
        } else {
            message.error("请求数据失败")
        }

        params.loading = false;
        changeTableParams && changeTableParams(params);

        //设置columns
        const columns = this.columns.map((col) => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    inputtype: 'text',
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                }),
            };
        });
        this.props.setColumns(columns);
    }

    render() {

        return (
            <div>
                <h3>上传文件信息</h3>
                <Divider>

                </Divider>
                <TableCustom>
                </TableCustom>

            </div>

        );
    }
}

export default Upload;