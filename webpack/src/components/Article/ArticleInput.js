import {Form, Input, Button, message, Select, Divider, Breadcrumb} from 'antd';
import React from 'react';

const FormItem = Form.Item;

import {Link} from "react-router-dom";
import UploadCustom from "../public/UploadCustom";
import CKEditorCustom from "../public/CKEditorCustom";
import {requestGet, requestPost} from '../public/Request';


class ArticleInputForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            loading: false,
            imageUrl: '',
            content: '',
            typeList: [],
            selectedItems: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChange = this.onChange.bind(this);

    }


//文章修改回调
    onChange(content) {
        this.props.form.setFieldsValue({content: content})

    }


    //图标修改后的回调
    handleChange = (imageUrl) => {
        this.props.form.setFieldsValue({title_image: imageUrl});
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll(async (err, values) => {
            if (!err) {


                let id = this.props.match.params.id;
                if (id) {
                    let params = {
                        ...values,
                        id
                    };
                    let resultData= await requestPost('article/update',params);

                    if (resultData.success == true) {

                        let params={
                            arrayCategory: values.arrayCategory,
                            article_id:id,
                        };
                        let resultData1= await requestPost('article-published-category/update',params);

                        console.log(resultData1);
                        if (resultData1) {
                            message.success('文章分类更新成功');
                        }
                        else {
                            message.warning('文章分类未更新');
                        }
                        message.success('文章更新成功', 1, function () {
                            this.props.history.push('/article');
                        }.bind(this));

                    }
                    else {
                        message.error('文章更新失败');
                    }
                } else {
                    let params = {
                        ...values
                    };
                    let resultData = await requestPost('article/add', params);


                    if (resultData.success == true) {

                        let params = {
                            arrayCategory: values.arrayCategory,
                            article_id: resultData.data,
                        };
                        let resultData1 = await requestPost('article-published-category/add', params);


                        if (resultData1.success == true) {
                            message.success('文章分类添加成功');
                        } else {
                            message.error('文章分类添加失败');
                        }

                        message.success('文章添加成功', 1, function () {

                            this.props.history.push('/article');
                        }.bind(this));

                    } else {
                        message.error('文章添加失败');
                    }
                }
            }
        });
    }


    async componentDidMount() {
        let id = this.props.match.params.id;

        let params = {};
        let resultData = await requestGet('article-category', params);
        if (resultData) {
            await this.setState({typeList: resultData});
        }

        if (id) {
            //获得某文章的分类数据

            let id = this.props.match.params.id;
            let paramsId = {article_id: id};
            let articlePublishedCategoryData = await requestGet('article-published-category/detail', paramsId);
            if (articlePublishedCategoryData) {
                let data = articlePublishedCategoryData;
                this.setState({selectValue: data});
                let arrayCategory = [];
                for (let i in data) {
                    arrayCategory.push('{"id":"' + data[i].category_id + '","title":"' + data[i].category_name + '"}')
                }
                this.props.form.setFieldsValue({
                    arrayCategory: arrayCategory
                })
            }


            //获得某文章的数据
            let articleParams = {id: id};
            let articleData = await requestGet('article/detail', articleParams);
            if (articleData) {
                let data = articleData;
                this.props.form.setFieldsValue({
                    title: data.title,
                    sub_title: data.sub_title,
                    abstract: data.abstract,
                    title_image: data.title_image,
                    content: data.content
                })
                this.setState({content: data.content});
            }
        }
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 20},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 10,
                },
            },
        };


        const {selectedItems} = this.state;
        const filteredOptions = this.state.typeList.filter(o => !selectedItems.includes(o));

        return (
            <Form onSubmit={this.handleSubmit}>
                <Breadcrumb separator=">">
                    <Breadcrumb.Item> <Link to={'/'}>首页</Link></Breadcrumb.Item>
                    <Breadcrumb.Item> <Link to={'/article'}>文章列表</Link></Breadcrumb.Item>
                    <Breadcrumb.Item>文章添加</Breadcrumb.Item>
                </Breadcrumb>
                <br/>
                <h3>文章添加</h3>
                <Divider></Divider>
                <FormItem
                    {...formItemLayout}
                    label="文章标题"
                >
                    {getFieldDecorator('title', {
                        rules: [{
                            required: true, message: '请输入标题!',
                        }]
                    })(
                        <Input placeholder={"请在此输入标题"}/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="子标题"
                >
                    {getFieldDecorator('sub_title')(
                        <Input placeholder={"请在此输入子标题"}/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="缩略图"
                >
                    {getFieldDecorator('title_image',)(
                        <UploadCustom uploadSuccess={this.handleChange}
                                      imageUrl={this.props.form.getFieldValue('title_image')}>

                        </UploadCustom>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="摘要"
                >
                    {getFieldDecorator('abstract')(
                        <Input/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="文章分类"
                >
                    {getFieldDecorator('arrayCategory', {
                        rules: [{
                            required: true, message: '请选择文章类型!', type: 'array'
                        }]
                    })(
                        <Select
                            mode="multiple"
                            placeholder="请输入招聘类型"
                            style={{width: '100%'}}
                        >
                            {filteredOptions.map(item => (
                                <Select.Option key={item.id}
                                               value={'{"id":"' + item.id + '","title":"' + item.title + '"}'}>
                                    {item.title}
                                </Select.Option>
                            ))}
                        </Select>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="文章内容"
                >
                    {getFieldDecorator('content', {
                        rules: [{
                            required: true, message: '请输入文章内容!',
                        }]
                    })(
                        <CKEditorCustom onChange={this.onChange}>

                        </CKEditorCustom>
                    )}
                </FormItem>

                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">添加</Button>
                </FormItem>
            </Form>
        );
    }
}

const ArticleInput = Form.create()(ArticleInputForm);
export default ArticleInput;