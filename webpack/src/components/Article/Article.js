import React from 'react';
import {Link} from 'react-router-dom';
import ModalCustom from '../public/ModalCustom';
import {
    message, Button, Divider, Popconfirm
} from 'antd';
import {TableCustom} from '../public/TableCustom'
import connect from "react-redux/es/connect/connect";
import {requestGet, requestPost} from '../public/Request';
import {serverUrl} from '../public/config'
import {
    setColumns, setVisible, setRichText, changeTableParams, setReqUrl
} from "../public/Reducer";
@connect(
    state => state,
    {setColumns, setVisible, setRichText, changeTableParams, setReqUrl}
)
export default class Article extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bordered: true,
            size: 'default',
            hasData: true,
            tableData: [],
            visible: false,
        };
        this.columns = [{
            title: 'id',
            dataIndex: 'id',
            key: 'id',
            sorter: true,
            width: 10,
        }, {
            title: '文章标题',
            dataIndex: 'title',
            key: 'title',
            width: 60,
            render: function (text, record, index) {
                return <a href={serverUrl + "article?id=" + record.id} target={"_blank"}>{text}</a>
            }
        }, {
            title: '子标题',
            dataIndex: 'sub_title',
            key: 'sub_title',
            width: 60,
        }, {
            title: '文章缩略图',
            dataIndex: 'title_image',
            key: 'title_image',
            width: 60,
            render: function (text, record, index) {
                return <a href={'javascript:'} onClick={this.showImgModal} className={text}><img src={text}
                                                                                                 style={{height: 40}}/></a>
            }.bind(this)
        }, {
            title: '摘要',
            dataIndex: 'abstract',
            key: 'abstract',
            width: 60,
        }, {
            title: '浏览次数',
            dataIndex: 'page_view',
            key: 'page_view',
            width: 60,
        }, {
            title: '创建者ID',
            dataIndex: 'user_id',
            key: 'user_id',
            width: 60,
        }, {
            title: '操作',
            dataIndex: 'edit',
            key: 'edit',
            width: 60,
            render: function (text, record, index) {
                return <div><Link to={"article-edit/" + record.id}>编辑 </Link>
                    <Popconfirm title="确定删除吗?" onConfirm={() => this.handleDelete(record.id)}>
                        <a href={"javascript:"}
                           id={record.id}>删除
                        </a>
                    </Popconfirm>
                </div>
            }.bind(this)
        }
        ];
    }


    handleDelete = async (key) => {
        const data = [...this.props.table.rows];
        let params = {id: key}
        let resultData = await requestPost('article/delete', params);
        if (resultData.success == true) {
            message.success('删除成功');
            this.props.changeTableParams({rows: data.filter(item => item.id !== key)});
        } else {
            message.error('删除失败');
        }
    }


    showImgModal=(e)=> {
        let imgElement = '<img src="' + e.target.src + '" width="800px"/>';
        this.props.setRichText(imgElement);
        this.props.setVisible(true)
    }


    async componentDidMount() {

        await this.props.setReqUrl("article/list");
        let {
            changeTableParams,
            table: {
                page,
                order
            }
        } = this.props;

        let params = {page, order}
        let data = await requestGet('article/list', params);
        if (data) {
            params.total = data.total;
            params.rows = data.data;
        } else {
            message.error("请求数据失败")
        }

        params.loading = false;
        changeTableParams && changeTableParams(params);

        const columns = this.columns;
        this.props.setColumns(columns);
    }


    render() {
        return (
            <div>
                <h3>文章列表</h3>
                <Divider></Divider>
                <Button type="primary" htmlType="submit" className="login-form-button loginBtn"
                        style={{marginBottom: 16}}>
                    <Link to={"article-add"}>添加文章信息</Link>
                </Button>

                <ModalCustom width={'1000px'} title={'图片预览'}>

                </ModalCustom>

                <TableCustom >

                </TableCustom>
            </div>
        );
    }
}
