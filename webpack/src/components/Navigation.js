import {Route } from "react-router";
import React, { Component } from 'react';
import { BrowserRouter,Link,HashRouter} from 'react-router-dom';
import Home from './Home/Home'
import About from './About/About'
import UserInput from "./User/UserInput";
import User from "./User/User";
import { Menu, Icon ,Layout } from 'antd';
import SlideInput from "./Slide/SlideInput";
import Slide from "./Slide/Slide";
import Recruit from "./Recruit/Recruit";
import RecruitInput from "./Recruit/RecruitInput";
import RecruitTypes from "./RecruitTypes/RecruitTypes";
import Article from "./Article/Article";
import ArticleInput from "./Article/ArticleInput";
import ArticleCategory from "./ArticleCategory/ArticleCategory";
import Nav from "./Nav/Nav";
import Footer1 from "./Footer1/Footer1";
import Footer1Input from "./Footer1/Footer1Input";
import Footer2Col from "./Footer2Col/Footer2Col";
import Company from "./Company/Company";
import EmailServiceConfig from "./EmailServiceConfig/EmailServiceConfig";
import Consult from "./Consult/Consult";
import Upload from "./Upload/Upload";
import Footer2 from "./Footer2/Footer2";
import {requestGet} from "./public/Request";
const {Content, Footer,Sider,Header} = Layout;


class Navigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: false,
            name:'',
            avatar:''
        };
        this.handleClick=this.handleClick.bind(this);
        this.toggle=this.toggle.bind(this);

    }

    async componentDidMount() {
        let name=window.localStorage.getItem("name");
        let avatar=window.localStorage.getItem("avatar");
        this.setState({name:name,avatar:avatar})
        let params={};
        let data= await requestGet('company/logo',params);
        if(data) {
            this.setState({logo: data.logo, corpName:data.c_name});

        }
        let selectKey = this.props.location.hash.substr(1);
        this.setState({selectKey:[selectKey]})
    }
    handleClick (e){
        console.log('click ', e);
        this.setState({
            current: e.key,

        });
    };
    toggle (e){
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    componentWillReceiveProps(nextProps, nextContext) {
        let selectKey = nextProps.location.hash.substr(1);
        this.setState({selectKey:[selectKey]})
    }


    render() {
        return (
            <HashRouter>
                <Layout>
                    <Sider
                        trigger={null}
                        collapsible
                        collapsed={this.state.collapsed}
                    >
                        <div className="logo" style={{height:64,display:'flex',justifyContent:'flex-start',alignItems:'center',paddingLeft:24}}>
                            <img  style={{height:50}} src={this.state.logo} alt=""/>
                        </div>
                        <Menu
                            defaultSelectedKeys={['/']}
                            defaultOpenKeys={['sub1']}
                            mode="inline"
                            theme="dark"
                            selectedKeys={this.state.selectKey}
                        >
                            <Menu.Item key="/">
                                <Link to={'/'}>
                                    <Icon type="home" />
                                    <span>首页</span>
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/user"><Link to={'/user'}><Icon type="user" /><span>用户</span></Link></Menu.Item>

                            <Menu.Item key="/slide"><Link to={"/slide"}><Icon type="picture" /><span>幻灯片</span></Link></Menu.Item>

                            <Menu.Item key="/recruit">
                                <Link to={'/recruit'}>
                                    <Icon type="solution" />
                                    <span>招聘</span>
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/recruit-type">
                                <Link to={'/recruit-type'}>
                                    <Icon type="block" />
                                    <span>招聘分类</span>
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/article">
                                <Link to={'/article'}>
                                    <Icon type="file-text" />
                                    <span>文章</span>
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/article-category">
                                <Link to={'/article-category'}>
                                    <Icon type="file" />
                                    <span>文章类别</span>
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/nav">
                                <Link to={'/nav'}>
                                    <Icon type="bars" />
                                    <span>导航</span>
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/footer1">
                                <Link to={'/footer1'}>
                                    <Icon type="project" />
                                    <span>页脚</span>
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/footer2">
                                <Link to={'/footer2'}>
                                    <Icon type="project" />
                                    <span>页脚2</span>
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/footer2-col">
                                <Link to={'/footer2-col'}>
                                    <Icon type="project" />
                                    <span>页脚2分栏</span>
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/upload">
                                <Link to={'/upload'}>
                                    <Icon type="upload" />
                                    <span>上传文件管理</span>
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/company">
                                <Link to={'/company'}>
                                    <Icon type="copyright" />
                                    <span>公司信息</span>
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/email-server-config">
                                <Link to={'/email-server-config'}>
                                    <Icon type="mail" />
                                    <span>邮件服务配置</span>
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/consult">
                                <Link to={'/consult'}>
                                    <Icon type="message" />
                                    <span>客户咨询</span>
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/about">
                                <Link to={'/about'}>
                                    <Icon type="info-circle" />
                                    <span>关于我们</span>
                                </Link>
                            </Menu.Item>
                        </Menu>
                    </Sider>
                    <Layout style={{minHeight:969}}>
                        <Header style={{ background: '#001529', padding: 0 ,display:"flex",justifyContent:"space-between",alignItems:"center" ,height:68}}>
                            <Icon
                                className="trigger"
                                type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                                onClick={this.toggle}
                            />
                            <div style={{color:"#fff",display:"flex",justifyContent:"space-between",alignItems:"center",marginRight:20,width:200}}>
                                <a href={'/login'}>退出登录</a>
                                <div>{this.state.name}</div>
                                <img style={{width:40,height:40}} src={this.state.avatar} alt=""/>
                            </div>
                        </Header>
                        <Content style={{
                            margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280,
                        }}
                        >
                            <Route exact path={"/"} component={Home} />
                            <Route path={'/about'} component={About} />
                            <Route path={'/user-add'} component={UserInput} />
                            <Route path={'/user'} component={User} />
                            <Route path={'/user-edit/:id'} component={UserInput} />
                            <Route path={'/slide-add'} component={SlideInput} />
                            <Route path={'/slide'} component={Slide} />
                            <Route path={'/slide-edit/:id/'} component={SlideInput} />
                            <Route path={'/recruit'} component={Recruit} />
                            <Route path={'/recruit-add'} component={RecruitInput} />
                            <Route path={'/recruit-edit/:id'} component={RecruitInput} />
                            <Route path={'/recruit-type'} component={RecruitTypes} />
                            <Route path={'/article'} component={Article} />
                            <Route path={'/article-add'} component={ArticleInput} />
                            <Route path={'/article-edit/:id'} component={ArticleInput} />
                            <Route path={'/article-category'} component={ArticleCategory} />
                            <Route path={'/nav'} component={Nav} />
                            <Route path={'/footer1'} component={Footer1} />
                            <Route path={'/footer1-add'} component={Footer1Input} />
                            <Route path={'/footer1-edit/:id'} component={Footer1Input} />
                            <Route path={'/footer2'} component={Footer2} />
                            <Route path={'/footer2-col'} component={Footer2Col} />
                            <Route path={'/company'} component={Company} />
                            <Route path={'/email-server-config'} component={EmailServiceConfig} />
                            <Route path={'/consult'} component={Consult} />
                            <Route path={'/upload'} component={Upload} />
                        </Content>

                        <Footer style={{ textAlign: 'center', height:50,padding:0}}>
                           Power by <a href="https://gitee.com/xdcenter/wocms" target={'_blank'}>Wocms</a>
                        </Footer>
                    </Layout>
                </Layout>
            </HashRouter>

        );
    }
}

export default  Navigation