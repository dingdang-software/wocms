import React from 'react';
import {Link} from 'react-router-dom';
import {
    Divider, message, Button, Popconfirm,
} from 'antd';
import ModalCustom from "../public/ModalCustom";
import {TableCustom} from "../public/TableCustom";
import connect from "react-redux/es/connect/connect";
import {setColumns, setVisible, setRichText, changeTableParams, setReqUrl} from "../public/Reducer";
import {requestGet, requestPost} from '../public/Request';


@connect(
    state => state,
    {setColumns, setVisible, setRichText, changeTableParams, setReqUrl}
)
export default class Slide extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bordered: true,
            size: 'default',
            hasData: true,
            tableData: [],
            visible: false,
            imgElement: ''
        };
        this.columns = [{
            title: 'id',
            dataIndex: 'id',
            sorter: true,
            key: 'id',
            width: 60,
        }, {
            title: '图片',
            dataIndex: 'image',
            key: 'image',
            width: 60,
            render: function (text, record, index) {
                return <a href={'javascript:'} onClick={this.showModal} className={text} target="view_window"><img
                    src={text} style={{height: 40}}/></a>
            }.bind(this)
        }, {
            title: '标题',
            dataIndex: 'title',
            key: 'title',
            width: 60,
        }, {
            title: '重定向链接',
            dataIndex: 'link_url',
            key: 'link_url',
            width: 60,
        }, {
            title: '描述',
            dataIndex: 'remark',
            key: 'remark',
            width: 60,
        }, {
            title: '权重',
            dataIndex: 'weight',
            key: 'weight',
            width: 60,
        },

            {
                title: '操作',
                dataIndex: 'edit',
                key: 'edit',
                width: 60,
                render: function (text, record, index) {
                    return <div><Link to={"slide-edit/" + record.id}>编辑</Link>
                        <Popconfirm title="确定删除吗?" onConfirm={() => this.handleDelete(record.id)}>
                            <a href="javascript:" style={{paddingLeft: 30}}> 删除</a>
                        </Popconfirm>
                    </div>
                }.bind(this)
            }
        ];

    }

    handleDelete = async (key) => {
        const data = [...this.props.table.rows];
        let params = {id: key}
        let resultData = await requestPost('slide/delete', params);
        if (resultData.success == true) {
            message.success('删除成功');
            this.props.changeTableParams({rows: data.filter(item => item.id !== key)});
        } else {
            message.error('删除失败');
        }
    }

    showModal = (e) => {
        let imgElement = '<img src="' + e.target.src + '" width="800px"/>';
        this.props.setRichText(imgElement);
        this.props.setVisible(true)
    }


    async componentDidMount() {
        await this.props.setReqUrl("slide/list");
        let {
            changeTableParams,
            table: {
                page,
                order
            }
        } = this.props;

        let params = {page, order}
        let data = await requestGet('slide/list', params);
        if (data) {
            params.total = data.total;
            params.rows = data.data;
        } else {
            message.error("请求数据失败")
        }

        params.loading = false;
        changeTableParams && changeTableParams(params);


        const columns = this.columns;
        this.props.setColumns(columns);
    }


    render() {
        return (
            <div>
                <h3>幻灯片信息</h3>
                <Divider></Divider>
                <Button type="primary" htmlType="submit" className="login-form-button loginBtn"
                        style={{marginBottom: 16}}>
                    <Link to={"slide-add"}>添加幻灯片</Link>
                </Button>

                <ModalCustom width={1000} title={'图片预览'}>
                </ModalCustom>

                <TableCustom >
                </TableCustom>

            </div>
        );
    }
}
