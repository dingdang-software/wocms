import {
    Form, Input, Button, message
    , InputNumber, Switch, Divider, Breadcrumb
} from 'antd';
import React from 'react';

const FormItem = Form.Item;
import {Link} from "react-router-dom";
import UploadCustom from "../public/UploadCustom";
import {requestGet, requestPost} from "../public/Request";

const {TextArea} = Input;


class SlideInputForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            loading: false,
            imgUrl: ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll(async (err, values) => {
            if (!err) {
                let params = {
                    title: values.title,
                    link_url: values.link_url,
                    remark: values.remark,
                    image: values.image,
                    weight: values.weight,
                    is_show: values.is_show
                };
                let id = this.props.match.params.id;
                if (!id) {
                    let resultData = await requestPost('slide/add', params);

                    if (resultData.success == true) {
                        message.success('添加成功', 1, function () {
                            this.props.history.push('/slide');
                        }.bind(this));
                    } else {
                        message.error('添加失败');
                    }
                } else {
                    params = {...params, id};
                    let resultData = await requestPost('slide/update', params);
                    if (resultData.success == true) {
                        message.success('更新成功', 1, function () {
                            this.props.history.push('/slide');
                        }.bind(this));
                    } else {
                        message.error('更新失败');
                    }
                }


            }
        });
    }


    //图标修改后的回调
    handleChange = (imageUrl) => {
        this.props.form.setFieldsValue({image: imageUrl})
    }


    async componentWillMount() {
        let id = this.props.match.params.id;
        if (id) {
            let params = {id};
            let data = await requestGet('slide/detail', params);
            if (data) {
                this.props.form.setFieldsValue({

                    title: data.title,
                    link_url: data.link_url,
                    remark: data.remark,
                    image: data.image,
                    weight: data.weight,
                    is_show: data.is_show

                });
            }
        }

    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 10},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 6,
                },
            },
        };


        return (
            <Form onSubmit={this.handleSubmit}>
                <Breadcrumb separator=">">
                    <Breadcrumb.Item> <Link to={'/'}>首页</Link></Breadcrumb.Item>
                    <Breadcrumb.Item> <Link to={'/slide'}>幻灯片列表</Link></Breadcrumb.Item>
                    <Breadcrumb.Item>幻灯片添加</Breadcrumb.Item>
                </Breadcrumb>
                <br/>
                <h3>幻灯片添加</h3>
                <Divider></Divider>
                <FormItem
                    {...formItemLayout}
                    label="幻灯片"
                >
                    {getFieldDecorator('image', {rules: [{required: true, message: '请选择幻灯片图片'}]})(
                        <UploadCustom uploadSuccess={this.handleChange}
                                      imageUrl={this.props.form.getFieldValue('image')}
                                      style={{height: 300}}
                        >

                        </UploadCustom>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="标题"
                >
                    {getFieldDecorator('title')(
                        <Input/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="描述"
                >
                    {getFieldDecorator('remark', {initialValue: "(可选)"})(
                        <TextArea rows={4}/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="重定向链接"
                >
                    {getFieldDecorator('link_url', {
                        rules: [{
                            required: true, message: '请输入重定向链接!',
                        }],
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="权重"
                >
                    {getFieldDecorator('weight', {
                        initialValue: "1",
                        rules: [{required: true, message: 'Please input your weight!'}]
                    })(
                        <InputNumber min={1}/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="是否显示"
                >
                    {getFieldDecorator('is_show', {
                        initialValue: "1",
                        rules: [{
                            required: true, message: 'Please input your is_show!',
                        }],
                    })(
                        <Switch defaultChecked/>,
                    )}
                </FormItem>

                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">添加</Button>
                </FormItem>
            </Form>
        );
    }
}

const SlideInput = Form.create()(SlideInputForm);
export default SlideInput;