import React, { Component } from 'react';
import {Form, Icon, Input, Button, message} from 'antd';
import {requestGet, requestPost} from "../public/Request";
const FormItem = Form.Item;
class ResetPassFrom extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email:'',
            password:'',
            securitycode:'',
            counting:false,
            count:60,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

     handleSubmit(e){
        e.preventDefault();
        this.props.form.validateFields(async(err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                let params={
                    email: values.email,
                    securityCode: values.securitycode,
                    password:values.password
                }
                let resultData= await requestPost('password/reset',params);
                if(resultData.success==true){
                    message.success('修改成功，两秒后跳转登录',2,function () {
                        window.location.href='/login';
                    });
                }
                else {
                    message.error('验证码错误或已失效');
                }

            }
        });
    }

    mailOnChange =(e)=>{
        console.log(e.target.value);
        this.setState({email:e.target.value})

    }

    countDown = () =>{
        const { count } = this.state;
        if ( count === 1) {
            this.clearInterval();
            this.setState( { counting: false });
        } else {
            this.setState( { counting: true, count: count-1});
        }
    }

    clearInterval=() =>{
        clearInterval(this.timer)
    }

    getSecurityCode =async()=>{
        let params={
            email: this.state.email
        }
        let resultData= await requestPost('securityCode',params);

        if(resultData.success==true){
            this.setState({counting:true})
            this.timer = setInterval(this.countDown, 1000)
        }

    }


    async componentDidMount() {
        // To disabled submit button at the beginning.
        this.props.form.validateFields();
        let params={};
        let data= await requestGet('company/logo',params);
        if(data) {
            this.setState({logo: data.logo, corpName:data.c_name});

        }
    }

    render() {
        const {
            getFieldDecorator
        } = this.props.form;

        return (
            <div className="loginContainer">
                <div className="login">

                    <img  style={{width:100,marginBottom:10}} src={this.state.logo} alt=""/>
                    <div className="corpName">密码重置</div>

                    <Form onSubmit={this.handleSubmit} style={{maxWidth: '300px'}}>
                        <FormItem>
                            {getFieldDecorator('email', {
                                rules: [{ required: true, message: '邮箱不能为空!' },{
                                    type: 'email', message: '输入邮箱的类型错误'}],
                            })(
                                <Input prefix={<Icon type="mail" style={{ fontSize: 13 }} />} placeholder="请输入邮箱" onChange={this.mailOnChange}/>
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('securitycode', {
                                rules: [{ required: true, message: '验证码不能为空!' },{
                                    min: 6, message: '输入验证码的应该为6位'}],
                            })(
                                <div style={{display: "flex"}}>
                                    <Input prefix={<Icon type="file-text"  style={{ fontSize: 13 }} />} placeholder="请输入验证码" maxLength={6}/>
                                    <Button type="primary" htmlType="submit" className={this.state.counting?'disabled':''+" login-form-button loginBtn"} style={{width: '50%',marginLeft:20}}
                                        onClick={this.getSecurityCode}
                                    >
                                        {this.state.counting? `${this.state.count}秒后重发` :'获取验证码'}
                                    </Button>
                                </div>

                            )}

                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: '新密码不能为空!' }],
                            })(
                                <Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="请输入新密码" />
                            )}
                        </FormItem>
                        <FormItem>
                            <a className="login-form-forgot" href="/login" style={{float:'right'}}>返回登录</a>
                            <Button type="primary" htmlType="submit" className="login-form-button loginBtn" style={{width: '100%'}}>
                                提交
                            </Button>
                        </FormItem>
                    </Form>
                </div>
            </div>
        );
    }
}
const ResetPass = Form.create()(ResetPassFrom);
export default ResetPass;