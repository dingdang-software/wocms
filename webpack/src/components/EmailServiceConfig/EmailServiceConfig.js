import {
    Form, Input, Button, message, Icon, Divider
} from 'antd';
import React from 'react';
const FormItem = Form.Item;
import {requestGet, requestPost} from "../public/Request";


let id = 0;
class EmailServiceConfigForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
        this.handleSubmit=this.handleSubmit.bind(this);
        this.add=this.add.bind(this);
    }

    handleSubmit (e) {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll(async  (err, values) => {
            if (!err) {

                let toEmail=[];
                for( let i in values.to_email){

                    if(values.to_email[i] !== "" && values.to_email[i] != undefined){
                        toEmail.push(values.to_email[i]);
                    }
                }
                let strToEmail=toEmail.join(",");


                let params={
                    id:this.state.id,
                    email:values.email,
                    from_name:values.from_name,
                    smtp_server:values.smtp_server,
                    port:values.port,
                    password:values.password,
                    to_email:strToEmail
                };

                let resultData= await requestPost('email-service-config/update',params);
                if (resultData.success == true) {
                    message.success('修改成功');
                }
                else {
                    message.error('修改失败');

                }
            }

        })
    }

    remove = (k) => {
        const { form } = this.props;
        const keys = form.getFieldValue('keys');
        if (keys.length === 1) {
            return;
        }

        form.setFieldsValue({
            keys: keys.filter(key => key !== k),
        });
    }

    add = () => {
        const { form } = this.props;
        const keys = form.getFieldValue('keys');
        const nextKeys = keys.concat(++id);
        form.setFieldsValue({
            keys: nextKeys,
        });
    }



    async componentWillMount() {
        let params={};
        let resultData= await requestGet('email-service-config',params);
        if(resultData){
            let data=resultData;
            let dataArr = data.to_email.split(",");
            let toEmail=[],keys=[];
            for(let i=1;i<=dataArr.length;i++){
                keys.push(i);
                toEmail[i]=dataArr[i-1];
            }
            this.props.form.setFieldsValue({
                email:data.email,
                keys:keys,
                from_name:data.from_name,
                smtp_server:data.smtp_server,
                port:data.port,
                password:data.password,
            });
            await this.setState({id:data.id});
            this.props.form.setFieldsValue({
                to_email:toEmail
            })
            id=keys.length;
        }
    }

    render() {
        const { getFieldDecorator ,getFieldValue} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 3 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 10 },
            },
        };

        const formNoLabelItemLayout = {
            wrapperCol: {
                xs: { span: 24 ,offset: 0,},
                sm: { span: 10 ,offset: 3,},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 5,
                },
            },
        };

        getFieldDecorator('keys', { initialValue:[] });
        let keys = getFieldValue('keys');
        const formItems = keys.map((k, index) => (
            <Form.Item
                {...(index === 0 ? formItemLayout : formNoLabelItemLayout)}
                label={index === 0 ? '接受邮件地址' : ''}
                required={false}
                key={k}
            >
                {getFieldDecorator(`to_email[${k}]`, {
                    initialValue:[] ,
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [{
                        required: true,
                        whitespace: true,
                        message: "请输入邮箱，或者删除此项",
                    },{
                        type:'email',
                        message: '输入邮箱的类型错误'
                    }],
                })(
                    <Input placeholder="接受邮件地址" style={{ width: '60%', marginRight: 8 }} />
                )}
                {keys.length > 1 ? (
                    <Icon
                        className="dynamic-delete-button"
                        type="minus-circle-o"
                        disabled={keys.length === 1}
                        onClick={() => this.remove(k)}
                    />
                ) : null}
            </Form.Item>
        ));


        return (
            <Form onSubmit={this.handleSubmit}>
                <h3>邮件服务器配置</h3>
                <Divider ></Divider>
                <FormItem
                    {...formItemLayout}
                    label="服务邮箱"
                >
                    {getFieldDecorator('email', {
                        rules: [{ required: true, message: '请输入服务邮箱!'}],
                    })(
                        <Input type={"mail"}/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="来自名字"
                >
                    {getFieldDecorator('from_name', {
                        rules: [{ required: true, message: '请输入来自名字!'}],
                    })(
                        <Input />
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="smtp服务地址"
                >
                    {getFieldDecorator('smtp_server', {
                        rules: [{ required: true, message: '请输入smtp服务地址!'}],
                    })(
                        <Input />
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="端口号"
                >
                    {getFieldDecorator('port', {
                        initialValue:25,
                        rules: [{ required: true, message: '请输入端口号!'}],
                    })(
                        <Input />
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="服务器密码"
                >
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: '请输入服务器密码!'}],
                    })(
                        <Input type="password"/>
                    )}
                </FormItem>
                <h3>邮件接收人配置</h3>
                <Divider ></Divider>
                {formItems}
                <Form.Item {...formNoLabelItemLayout}>
                    <Button type="dashed" onClick={this.add} style={{ width: '60%' }}>
                        <Icon type="plus" /> Add field
                    </Button>
                </Form.Item>
                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">修改</Button>
                </FormItem>
            </Form>
        );
    }
}

const EmailServiceConfig = Form.create()(EmailServiceConfigForm);
export default EmailServiceConfig;