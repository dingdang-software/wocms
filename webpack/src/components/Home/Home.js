import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Card, Icon, Divider, Button, message} from "antd";
import {requestGet} from "../public/Request";

const {Meta} = Card;

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            homeInfo: []
        }
    }

    clearSession = async () => {
        let params = {};
        let that = this;
        let data = await requestGet('session/clear', params);

        message.success("成功清除" + data + "条", 1, function () {
            that.fetchHomeData();
        });

    }

    async componentDidMount() {
        this.fetchHomeData();

    }

    fetchHomeData = async () => {
        let params = {};
        let data = await requestGet('home', params);
        if (data) {
            this.setState({homeInfo: data})
        }
    }

    render() {
        return (
            <div>
                <h2 style={{marginBottom: 20}}>欢迎来到后台管理系统</h2>
                <Divider></Divider>
                <div style={{display: 'flex', flexWrap: 'wrap'}}>
                    <Link to={'/user'} style={{display: 'block', width: '23%', marginBottom: 16, marginRight: 20,}}>
                        <Card style={{cursor: 'pointer', width: '100%', fontSize: 30}}>
                            <Meta
                                avatar={<Icon type="user" style={{fontSize: 30, color: '#308dd5'}}/>}
                                style={{fontSize: 28, marginBottom: 20, display: 'flex', alignItems: 'center'}}
                                title={'用户数'}
                            />
                            {this.state.homeInfo['userNum']}
                        </Card>
                    </Link>
                    <Link to={'/article'} style={{display: 'block', width: '23%', marginBottom: 16, marginRight: 20,}}>
                        <Card style={{cursor: 'pointer', width: '100%', fontSize: 30}}>
                            <Meta
                                avatar={<Icon type="file-text" style={{fontSize: 30, color: '#f06803'}}/>}
                                style={{fontSize: 28, marginBottom: 20, display: 'flex', alignItems: 'center'}}
                                title={'文章数'}
                            />
                            {this.state.homeInfo['articleNum']}
                        </Card>
                    </Link>
                    <Link to={'/consult'} style={{display: 'block', width: '23%', marginBottom: 16, marginRight: 20,}}>
                        <Card style={{cursor: 'pointer', width: '100%', fontSize: 30}}>
                            <Meta
                                avatar={<Icon type="message" style={{fontSize: 30, color: '#c8146f'}}/>}
                                style={{fontSize: 28, marginBottom: 20, display: 'flex', alignItems: 'center'}}
                                title={'咨询信息数'}
                            />
                            {this.state.homeInfo['consultNum']}
                        </Card>
                    </Link>
                    <Link to={'/recruit'} style={{display: 'block', width: '23%', marginBottom: 16, marginRight: 20,}}>
                        <Card style={{cursor: 'pointer', width: '100%', fontSize: 30}}>
                            <Meta
                                avatar={<Icon type="solution" style={{fontSize: 30, color: '#308dd5'}}/>}
                                style={{fontSize: 28, marginBottom: 20, display: 'flex', alignItems: 'center'}}
                                title={'招聘数'}
                            />
                            {this.state.homeInfo['recruitNum']}
                        </Card>
                    </Link>
                    <Link to={'/slide'} style={{display: 'block', width: '23%', marginBottom: 16, marginRight: 20,}}>
                        <Card style={{cursor: 'pointer', width: '100%', fontSize: 30}}>
                            <Meta
                                avatar={<Icon type="picture" style={{fontSize: 30, color: '#3ca921'}}/>}
                                style={{fontSize: 28, marginBottom: 20, display: 'flex', alignItems: 'center'}}
                                title={'幻灯片数'}
                            />
                            {this.state.homeInfo['slideNum']}
                        </Card>
                    </Link>
                    <Link to={'/upload'} style={{display: 'block', width: '23%', marginBottom: 16, marginRight: 20,}}>
                        <Card style={{cursor: 'pointer', width: '100%', fontSize: 30}}>
                            <Meta
                                avatar={<Icon type="upload" style={{fontSize: 30, color: '#e6594f'}}/>}
                                style={{fontSize: 28, marginBottom: 20, display: 'flex', alignItems: 'center'}}
                                title={'文件数'}
                            />
                            {this.state.homeInfo['fileUploadNum']}
                        </Card>
                    </Link>
                    <div style={{display: 'block', width: '23%', marginBottom: 16, marginRight: 20,}}>
                        <Card style={{cursor: 'pointer', width: '100%', fontSize: 30}}>
                            <Meta
                                avatar={<Icon type="upload" style={{fontSize: 30, color: '#e6594f'}}/>}
                                style={{fontSize: 28, marginBottom: 20, display: 'flex', alignItems: 'center'}}
                                title={'会话数量'}
                            />
                            {this.state.homeInfo['sessionNum'] + 1}
                            <Button onClick={this.clearSession} style={{marginLeft: 10}}>清理</Button>
                        </Card>
                    </div>
                </div>
                <Card style={{cursor: 'pointer', marginTop: 40, marginRight: 20, width: '100%', fontSize: 16}}>
                    <Meta
                        avatar={<Icon type="info-circle" style={{fontSize: 30, color: '#000'}}/>}
                        style={{fontSize: 28, marginBottom: 5, display: 'flex', alignItems: 'center'}}
                        title={'系统信息'}
                    />

                    {'系统信息: ' + this.state.homeInfo['osVersion']}<br/>
                    {'ip地址: ' + this.state.homeInfo['ip']}<br/>
                    {'PHP版本: ' + this.state.homeInfo['phpVersion']}<br/>
                    {'服务器信息: ' + this.state.homeInfo['webServer']}<br/>
                    {'time_zone: ' + this.state.homeInfo['timezone']}
                </Card>
            </div>

        );
    }
}