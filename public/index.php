<?php

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

session_start();

require __DIR__ . '/../vendor/autoload.php';


//config
require __DIR__ . '/../config/constants.php';
require __DIR__ . '/../config/database.php';


// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);


// Set up dependencies
require __DIR__ . '/../src/dependencies.php';


// Register middleware
require __DIR__ . '/../src/middleware.php';


header('Access-Control-Allow-Origin:*');
header('Access-Control-Allow-Methods:GET,POST,PUT,DELETE,PATCH,OPTIONS');
header('Access-Control-Allow-Headers:x-requested-with,content-type,accept, origin, authorization,sess_id,access-token');


// Register routes
foreach (glob(__DIR__ . "/../src/routes/*.php") as $fileName) {
    require $fileName;
}


// Run app
$app->run();
