<?php
/**
 * Created by PhpStorm.
 * User: tsilo
 * Date: 2018/12/18
 * Time: 11:02
 */

require __DIR__ .'/../vendor/autoload.php';

use Doctrine\Common\Inflector\Inflector;

//class_name_a
class ClassNameA{

    public function getName(){
        $ref = new ReflectionClass($this);
        return $ref->getName();
    }


    public function getTableName(){
        //$str = preg_replace("/[A-Z]/","_$1",$this->getName());
        //return strtolower(trim($str,"_"));
        return Inflector::tableize($this->getName());
    }

}
$cls = new ClassNameA();

echo $cls->getTableName();