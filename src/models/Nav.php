<?php
/**
 * 导航
 * User: Administrator
 * Date: 2018\12\14 0014
 * Time: 16:09
 */

class Nav extends Model
{
    //属性 记录id,父级id，标题，是否显示，排序使用，链接，创建时间
    public $id,$parent_nav_id, $title, $is_show, $level, $link_url, $created_at;

    /**
     * 比较权重函数
     * @return 0|-1|1
     */
    protected function cmpLevel($a, $b)
    {
        if ($a["level"] == $b["level"]) {
            return 0;
        }
        return ($a["level"] < $b["level"]) ? -1 : 1;
    }

    /**
     * 查找所有能显示的导航数据
     * @return array
     */
    public function fetchAllCanShow(){



        //设定查询条件，显示为1
        $condition = "is_show=1";

         //查询
        $result=$this->fetchAllBy($condition);

        //遍历结果得到一个父导航的数组
        foreach($result as $key=>$item){
            if($item['parent_nav_id']==0){
                $arr[]=$item;
            }
        }

        //对父导航排序
        usort($arr, array('Nav','cmpLevel'));


        $arr1=array(array());
        //生成子导航二维数组
        foreach($arr as $key=>$item){
            $pid=$item['id'];
            foreach ($result as $key1=>$item1){
                if($pid==$item1['parent_nav_id']){
                    $arr1[$key][]=$item1;
                }
            }
        }

        //对子导航排序
        foreach($arr1 as $key=>$item){
            usort($arr1[$key], array('Nav','cmpLevel'));
        }

        $arr2=array(array());
        foreach ($arr as $key=>$item1){
            $obj=(object)null;
            $obj->title=$item1['title'];
            $obj->link_url=$item1['link_url'];
            $arr2[$key][]=$obj;

        }

        //父子导航结合生成新的二维数组
        foreach($arr1 as $key=>$item){
            foreach ($item as $key1=>$item1){
                $obj=(object)null;
                $obj->title=$item1['title'];
                $obj->link_url=$item1['link_url'];
                $arr2[$key][]=$obj;

            }
        }


        return $arr2;

    }


}