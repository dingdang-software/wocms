<?php
/**
 * 主页新闻块展示的文章类型
 * User: Lu
 * Date: 2019/1/4
 * Time: 14:04
 */

class BlockShowArticleCategory extends Model
{
    public $id,$category_id,$category_name;

    public function fetchAll(){
        $statement='select a.id, a.category_id, a.category_name, b.article_id, c.title, c.sub_title,c.title_image,c.abstract, c.content,c.page_view
from block_show_article_category a,article_published_category b,article c
where a.category_id=b.category_id and b.article_id=c.id';
        $data = DB::Pdo()->query($statement)->fetchall(PDO::FETCH_ASSOC);
        if($data){
            foreach ($data as  $key => $value){
                $id=$value['id'];
                $arr[$id][]=$value;
            }
        }
        return $arr;

    }
}