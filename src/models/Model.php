<?php

/**
 * 模型基类
 * User: ALU
 * Date: 2018/12/14
 * Time: 10:49
 */

use Doctrine\Common\Inflector\Inflector;

abstract class Model
{
    /**
     * 返回一个新的实例
     * @return mixed
     */
    public static function getInstance()
    {
        return new static();
    }

    /**
     * 获取类对应的表明，需要遵循规范才有效
     * @return string
     */

    protected function getRefTableName()
    {
        //new self(); == new $this; //new static(); //当前类
        $ref = new ReflectionClass(new static());
        return Inflector::tableize($ref->getName());
    }

    /**
     * 根据记录id获取一条记录
     * @return array|null
     */
    public function findOneById()
    {
        $row = DB::Connect()->{$this->getRefTableName()}()->where("id", $this->id)->fetch();
        return $row ? iterator_to_array($row) : null;
//        if ($row && $row instanceof Traversable) {
//            $obj = new static();
//            foreach ($row as $key => $val) {
//                property_exists($obj, $key) ? $obj->$key = $val : null;
//            }
//            return $obj;
//        }
    }

    /**
     * 根据id删除一条记录，成功返回删除数量，失败返回0
     * @return int|bool
     */
    public function deleteById()
    {
        return DB::Connect()->{$this->getRefTableName()}()->where("id", $this->id)->delete();
    }

    /**
     * 根据字段属性更改记录
     * @param string $field
     * @return mixed
     */
    public function updateBy($field = "id")
    {
        //过滤掉属性为null的属性
        $data = array_filter(get_object_vars($this), function ($val) {
            return !is_null($val);
        });
        unset($data["id"], $data["created_at"]); //去掉这些数据，放置更改

        $num = DB::Connect()->{$this->getRefTableName()}()->where($field, $this->$field)->update($data);
        return $num;
    }


    /**
     * 根据条件统计数量
     * @param string|array $exp 统计条件
     * @return int
     */
    protected function count($exp = "1")
    {
        return (int)DB::Connect()->{$this->getRefTableName()}()->where($exp)->count(1);
    }

    /**
     * 统计表中的所有记录数量
     * @return int
     */
    public function countAll()
    {
        return $this->count("1");
    }

//    /**
//     * 添加记录，记住属性千万不能是数据库中不存在的字段
//     * @param Model $obj 实例 instanceof Model
//     * @return int
//     */
//    public static function addNewRecord($obj)
//    {
//        //过滤掉属性为null的属性
//        $data = array_filter(get_object_vars($obj),function($val){
//            return !is_null($val);
//        });
//        //插入记录
//        $row = DB::Connect()->{$obj->getRefTableName()}()->insert($data);
//        return $row ? (isset($row["id"]) ? $row["id"] : 1) : 0;
//    }

    /**
     * 实例添加记录方式，静态方法和这个选一种
     * @return int
     */
    public function addNewRecord()
    {
        //过滤掉属性为null的属性
        $data = array_filter(get_object_vars($this), function ($val) {
            return !is_null($val);
        });
        //插入记录
        $row = DB::Connect()->{$this->getRefTableName()}->insert($data);
        return $row ? $this->id = $row["id"] : 0;
    }


    /**
     * 根据条件查找相关所有记录
     * @param string|array $exp
     * @return array|null
     */
    protected function fetchAllBy($exp = '1')
    {
        $rows = DB::Connect()->{$this->getRefTableName()}()->where($exp);
        if ($rows && $rows instanceof Traversable) {
            $arr = null;
            foreach ($rows as $row) {
                $arr[] = iterator_to_array($row);
            }
            return $arr;
        }
        return null;
    }


    /**
     * 根据页数和每条限制页数查找相关所有记录
     * @param int $page
     * @param int $limit
     * @param string $sortBy
     * @param string $order
     * @return array
     *
     */
    public function fetchList($page = 1, $limit = 10,$sortBy = "id", $order = "desc")
    {
        $offset = ($page - 1) * $limit;
        $data = [];
        $total = $this->countAll();
        if (!$total || $total >= $offset) {
            $rows = DB::Connect()->{$this->getRefTableName()}()->order("{$sortBy} {$order}")->limit($limit, $offset);
            if ($rows && $rows instanceof Traversable) {
                foreach ($rows as $row) {
                    $data[] = iterator_to_array($row);
                }
            }
        }
        return ["total" => $total, "data" => $data];
    }


    /**
     * 查找所有记录
     * @return array|null
     */
    public function fetchAll()
    {
        return $this->fetchAllBy("1");
    }

    //todo 添加更多方法

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }




//    public function cmpWeight($a, $b)
//    {
//        if ($a["weight"] == $b["weight"]) {
//            return 0;
//        }
//        return ($a["weight"] > $b["weight"]) ? -1 : 1;
//    }
}