<?php

/**
 * 随机工具类
 * User: ALU
 * Date: 2018/9/9
 * Time: 16:26
 */
//namespace Doujin\Utils;

class Random
{
    /**
     * 随机数字和字符串
     * @param int $len 字符串长度
     * @param bool $case 大小写敏感
     * @return string
     */
    public static function genStr($len, $case = true){
        $retStr = '';
        $str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';//62个字符
        $strLen = 62;
        //$str = str_shuffle($str);
        for ($i = 0; $i < $len; $i++) {
            $index = random_int(0, $strLen);
            $retStr .= mb_substr($str, $index, 1);
        }
        return !$case ? strtolower($retStr) : $retStr;
    }

    /**
     * 随机字符串，方法2
     * @param int $len 字符串长度
     * @param bool $case 大小写敏感
     * @return string
     */
    public static function genStr2($len, $case = true) {
        $str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';//62个字符
        $strLen = 62;
        while ($len > $strLen) {
            $str .= $str;
            $strLen += 62;
        }
        $str = str_shuffle($str);
        $retStr = mb_substr($str, 0, $len);
        return !$case ? strtolower($retStr) : $retStr;
    }

    /**
     * 随机纯数字字符串
     * @param int $len 字符串长度
     * @return string 字符串
     */
    public static function genNetNumberStr($len) {
        $retStr = '';
        $numStr = '0123456789';
        $numStr = str_shuffle($numStr);
        for ($i = 0; $i < $len; $i++) {
            $num = random_int(0, 9);
            $retStr .= mb_substr($numStr, $num, 1);
        }
        return $retStr;
    }

    /**
     * 生成32位长度的随机token字符串
     * @param string $str 参与生成字符串的字符
     * @return string
     */
    public static function genToken($str = '') {
        $s = $str ? $str : self::genStr(8);
        return md5($s . SECURE_AUTH_SALT . microtime());
    }

    /**
     * 随机获取数组中的指定数量元素
     * @param array $arr 数组
     * @param int $size 数量
     * @return array 获取的元素
     */
    public static function getArrayRand(array $arr, $size=10){
        $ret = [];
        $keys = array_rand($arr, $size);
        foreach ($keys as $key) {
            $ret[$key] = $arr[$key];
        }
        return $ret;
        //方案二：
        //shuffle($arr);
        //return array_slice($arr, 0, $size);
    }
}