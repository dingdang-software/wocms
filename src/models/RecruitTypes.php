<?php

/**
 * 招聘类型
 * User: ALU
 * Date: 2018/12/18
 * Time: 19:51
 */
class RecruitTypes extends Model
{
    //属性：记录id，招聘类型，记录数量，最近更新，创建时间
    public $id,$o_type,$cnt,$updated_at,$created_at;

    /**
     * RecruitType constructor.
     * 构造器
     */
    public function __construct()
    {
        $this->updated_at = $this->created_at = time();
    }

    /**
     * 获取所有的分类
     * @return array
     */
    public function fetchAll()
    {
        $data = $this->fetchAll();
        return $data;
    }

    /**
     * 获取列表
     * @param int $page
     * @param int $limit
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function fetchList($page=1, $limit=20,$sort = "id",$order="desc"){
        $offset = ($page - 1) * $limit;
        //todo 完善，分页，用于后台管理
        $rows = DB::Connect()->recruit_types()->order("$sort")->limit($limit,$offset);
        $data = [];
        if ($rows && $rows instanceof Traversable){
            foreach ($rows as $row) {
                $data[] = iterator_to_array($row);
            }
        }
        return $data;
    }

}