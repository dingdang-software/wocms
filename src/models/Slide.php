<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018\12\15 0015
 * Time: 11:54
 */

class Slide extends Model
{

    //属性：记录id，幻灯片连接地址，标题，重定向地址，备注，权重，是否展示0-1，创建管理者id，最近更新，创建时间
    public $id, $image, $title, $link_url, $remark, $weight, $is_show, $user_id, $updated_at, $created_at;

    public function __construct()
    {
        $this->updated_at = $this->created_at = time();
    }


    /**
     * 比较权重函数
     * @return 0|-1|1
     */
    protected function cmpWeight($a, $b)
    {
        if ($a["weight"] == $b["weight"]) {
            return 0;
        }
        return ($a["weight"] < $b["weight"]) ? -1 : 1;
    }

    /**
     * 获取所有可以展示的内容
     * @return array|null
     */
    public function fetchAllCanShow()
    {

        $result =  $this->fetchAllBy(["is_show"=>1]);

        // 根据权重排序
        if ($result){
            usort($result, array('Slide','cmpWeight'));
        }
        return $result;
    }


}