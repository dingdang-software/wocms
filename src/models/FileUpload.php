<?php
/**
 * 文件上传
 * User: Lu
 * Date: 2018/12/25
 * Time: 13:47
 */

class FileUpload extends Model
{
    public $id,$name,$uri,$type,$size,$updated_at,$created_at;

    public function __construct()
    {
        $this->updated_at = $this->created_at = time();
    }



    public function deleteDataAndFileById()
    {
        $findData=$this->findOneById();
        if($findData){
            $unlinkData=unlink(__DIR__.'/../../public/'.$findData['uri']);
            if($unlinkData) {
                return  $this->deleteById();
            }
        }

        return false;
    }
}