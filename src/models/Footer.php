<?php
/**
 * 页脚
 * User: Administrator
 * Date: 2018\12\15 0015
 * Time: 17:30
 */

class Footer
{

    public $id,$display_position,$col_id,$col_title,$weight,$link_txt,$link_url,$created_at;
    /**
     * 比较权重函数
     * @return 0|-1|1
     */
    protected function cmpWeight($a, $b)
    {
        if ($a["weight"] == $b["weight"]) {
            return 0;
        }
        return ($a["weight"] < $b["weight"]) ? -1 : 1;
    }

    /**
     * Recruit constructor.
     * 构造器创建时间戳
     */
    public function __construct()
    {
       $this->created_at = time();
    }


    /**
     * 查找footer_2所有内容 返回一个二维数组
     * @return array|null
     **/
    public function fetchAllFooter2WithArray(){

        //查找footer2 分类，放入一个数组中
        $result=DB::Connect()->footer_2_col();

        if($result){
            foreach ($result as $key=>$item) {
                $data[] = iterator_to_array($item);
            }
        }


        //分类数组权重排序
        usort($data, array('Footer','cmpWeight'));


        //查找foot2内容，放入一个数组中
        $result1=DB::Connect()->footer_2()
        ->order('weight');
        if($result1){
            foreach ($result1 as $key=>$item) {
                $data1[] = iterator_to_array($item);
            }
        }


        $arr=array(array());

        //合并两个数组
        foreach ($data as $key=>$item){
            $pid=$item['id'];
            $obj=(object)null;
            $obj->title=$data[$key]['col_title'];
            $obj->link_url='#';
            $arr[$key][]=$obj;
            foreach ($data1 as $key1=>$item1){
                if($pid==$item1['col_id']){
                    $obj=(object)null;
                    $obj->title=$item1['link_txt'];
                    $obj->link_url=$item1['link_url'];
                    $arr[$key][]=$obj;
                }
            }
        }
        return $arr;
    }

    /**
     * 根据数据库名查找所有内容
     * @param $dataBase
     * @return array
     */
    public function fetchAll($dataBase){

        $result=DB::Connect()->$dataBase();
        if($result){
            foreach ($result as $key=>$item) {
                $data[] = iterator_to_array($item);
            }
        }


        return $data;
    }

    /**
     * 根据数据库名修改某条内容
     * @param $dataBase
     * @return mixed
     */
    public function updateById($dataBase){
        $data = array_filter(get_object_vars($this),function($val){
            return !is_null($val);
        });
        unset($data["id"],$data["created_at"]); //去掉这些数据，放置更改

        $num = DB::Connect()->$dataBase()->where('id',$this->id)->update($data);
        return $num;
    }

    /**
     * 根据id删除一条记录，成功返回删除数量，失败返回0
     * @param $dataBase
     * @return mixed
     */
    public function deleteById($dataBase)
    {
        return DB::Connect()->$dataBase()->where("id", $this->id)->delete();
    }


    /**
     * 添加记录
     * @return int
     */
    public function addNewRecord($dataBase)
    {
        //过滤掉属性为null的属性
        $data = array_filter(get_object_vars($this),function($val){
            return !is_null($val);
        });
        //插入记录
        $row = DB::Connect()->$dataBase()->insert($data);
        return $row ? $this->id = $row["id"] : 0;
    }

    /**
     * 根据记录id获取一条记录
     * @return array|null
     */
    public function findOneById($dataBase){
        $row = DB::Connect()->$dataBase()->where("id", $this->id)->fetch();
        return $row ? iterator_to_array($row) : null;
    }



    /**
     * 查找footer1和footer2所有内容
     * @return array|null
     **/
    public function fetchFooters(){
        $footer2List=$this->fetchAllFooter2WithArray();
        $footer1List=$this->fetchAll('footer_1');
        return ['footer2List'=>$footer2List,'footer1List'=>$footer1List];
    }


    /**
     * 通过分页查找某页数据
     * @param $page
     * @param int $limit
     * @param string $sortBy
     * @param string $order
     * @param $dataBase
     * @return array
     */

    public function fetchAllWithPagination($page,$limit=10,$sortBy='id',$order='id',$dataBase){

        $offset = ($page - 1) * $limit;
        $data = [];
        $total =  (int)DB::Connect()->$dataBase()->count(1);
        if (!$total || $total >= $offset) {
            $rows = DB::Connect()->$dataBase()->order("id {$order}")->limit($limit, $offset);
            if ($rows && $rows instanceof Traversable) {
                foreach ($rows as $row) {
                    $data[] = iterator_to_array($row);
                }
            }
        }
        return ["total" => $total, "data" => $data];
    }

}