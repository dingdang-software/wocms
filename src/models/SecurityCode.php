<?php
/**
 * 验证码表
 * User: Lu
 * Date: 2019/1/5
 * Time: 11:16
 */

class SecurityCode extends Model
{
    public $id,$security_code,$email,$expired,$created_at;

    public function __construct()
    {
         $this->created_at = time();
         $this->expired=time()+60*5;
    }

    public function fetchOneByEmailAndSecurityCode(){
        $rows = DB::Connect()->security_code()->where('email',$this->email)->where('security_code',$this->security_code)->fetch();
        if($rows['expired']>time()){
            $this->id= $rows['id'];
            $this->deleteById();
            return $rows['id'];
        }
        else{
            $this->id= $rows['id'];
            $this->deleteById();
            return false;
        }
    }
}