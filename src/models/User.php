<?php

/**
 * 用户类
 * User: ALU
 * Date: 2018/12/14
 * Time: 10:44
 */
class User extends Model
{
    public $id,$avatar,$name,$email,$phone,$sex,$password,$role_level,$updated_at,$created_at;

    public function __construct()
    {
        $this->updated_at = $this->created_at = time();
    }

    //todo 添加逻辑和方法

    /**
     * 查找用户账号密码是否匹配
     * @return boolean|array
     */
    public function fetchOneCanLogin($account)
    {
        $preg_email='/^[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*@([a-zA-Z0-9]+[-.])+([a-z]{2,5})$/ims';
        $preg_phone='/^1[34578]\d{9}$/ims';
         if(preg_match($preg_email,$account)){
             $result=$this->fetchAllBy(["email"=>$account,"password"=>$this->password]);//$condition="email==$account AND password=$this->password";
        }elseif(preg_match($preg_phone,$account)){
             $result=$this->fetchAllBy(["phone"=>$account,"password"=>$this->password]);//$condition="phone==$account AND password=$this->password";
        }
         else{
             return false;
         }
         return $result;
    }
    /**
     * 查找用户邮箱和电话是否存在
     * @return boolean|array
     */
    public function fetchAllPhoneAndEMailRepeat(){

        $emailData=$this->fetchAllBy(["email"=>$this->email]);
        $phoneData=$this->fetchAllBy(["phone"=>$this->phone]);
        if($emailData){
            if($emailData[0]['id']!=$this->id){
                return true;
            }
        }
        if($phoneData){
            if($phoneData[0]['id']!=$this->id){
                return true;
            }
        }
        return false;
    }

    /**
     * 发送邮件
     * @return bool|string
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public function sendSecurityCodeToEmail(){
        $preg_email='/^[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*@([a-zA-Z0-9]+[-.])+([a-z]{2,5})$/ims';
        if(preg_match($preg_email,$this->email)){
           $mail= new Mail();
           $mailHandle=$mail->init_mailer();
           $random = Random::genNetNumberStr(6);
          if($mail->sendMail($mailHandle,$this->email,'亲爱的用户','帐号找回密码验证','本次找回密码的验证码是'.$random)){
              return $random;
          }
        }
        return false;
    }



}