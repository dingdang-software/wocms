<?php

/**
 * 招聘
 * User: Lu Lizhi
 * Date: 2018/12/14
 * Time: 10:47
 */
class Recruit extends Model
{
    //属性：记录id，职位，招聘类型id，招聘类型，工作类型，薪水，招聘人数，工作地点，职位描述，发布时间，有效期，最近更新，创建时间
    public $id, $job, $recruit_type_id, $recruit_type, $job_nature, $salary, $num,
        $work_at, $skill_requirement, $published_at, $expired_at, $updated_at, $created_at;

    /**
     * Recruit constructor.
     * 构造器创建时间戳
     */
    public function __construct()
    {
        $this->updated_at = $this->created_at = time();
    }

    /**
     * 静态方法添加记录add()
     * @param Recruit $recruit
     * @return int
     */
//    public static function addNewRecord(Recruit $recruit)
//    {
//        //过滤掉属性为null的属性
//        $data = array_filter(get_object_vars($recruit),function($val){
//            return !is_null($val);
//        });
//        //插入记录
//        $row = DB::Connect()->{$recruit->getRefTableName()}()->insert($data);
//        return $row ? $row["id"] : 0;
//    }


    /**
     * 获取职位信息类表
     * @param int $page
     * @param int $limit
     * @param string $sortBy
     * @param string $order
     * @return array
     *
     */
    public function fetchList($page = 1, $limit = 3, $sortBy = "id", $order = "desc")
    {
        $offset = ($page - 1) * $limit;
        $data = [];
        if (!$this->recruit_type_id) {
            $condition = "1";
            $total = $this->countAll();
        } else {
            $condition = "recruit_type_id = $this->recruit_type_id";
            $total = $this->count(["recruit_type_id" => $this->recruit_type_id]);
        }

        $isEnd = ($total - $limit) <= $offset;
        if (!$total || $total >= $offset) {
            $rows = DB::Connect()->recruit()->where($condition)->order("{$sortBy} {$order}")->limit($limit, $offset);
            if ($rows && $rows instanceof Traversable) {
                foreach ($rows as $row) {
                    $data[] = iterator_to_array($row);
                }
            }
        }
        $totalPage = ceil($total / $limit);

        return ["total" => $total, "is_end" => $isEnd, "total_page" => $totalPage, "rows" => $data];
    }



}