<?php

/**
 * Class DB
 * 连接池可以使用swoole实现。参考网上教程
 */
class DB
{
    //连接实例，redis连接实例，pdo连接实例，notorm连接实例
    protected static $redisInstance, $pdoInstance, $notormInstance;

    /**
     * 创建pdo实例
     */
    private static function createPdoInstance()
    {
        try {
            self::$pdoInstance = new PDO("mysql:host=" .MYSQL_HOST  . ";dbname=" . MYSQL_DB_NAME, MYSQL_USER, MYSQL_PASSWORD,
                [
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8;",
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
                ]);
        } catch (PDOException $e) {
            //throw $e;
            exit($e->getMessage()); //die($e->getMessage());
        }
    }

    /**
     * 连接redis缓存数据据库，创建实例
     * @param string $host
     * @param int $port
     */
    private static function createRedisInstance($host = REDIS_DEFAULT_HOST, $port = REDIS_DEFAULT_PORT)
    {
        self::$redisInstance = new Redis();
        self::$redisInstance->connect($host, $port);
    }

    /**
     * 开启事务
     */
    public static function StartTransaction()
    {
        self::Connect()->transaction = "BEGIN";
    }

    /**
     * 提交
     */
    public static function Commit()
    {
        self::Connect()->transaction = "COMMIT";
    }

    /**
     * 事务回滚
     */
    public static function Rollback()
    {
        self::Connect()->transaction = "ROLLBACK";
    }

    /**
     * NotORM数据库链接返回句柄
     * @return \NotORM
     */
    public static function Connect()
    {
        if (!self::$notormInstance || !self::$pdoInstance instanceof NotORM) {
            (!self::$pdoInstance || !self::$pdoInstance instanceof PDO) ? self::createPdoInstance() : null;
            self::$notormInstance = new NotORM(self::$pdoInstance);
            //defined("DEBUG") ? self::$notormInstance->debug = DEBUG : null;
            self::$notormInstance->debug = DEBUG;
        }
        return self::$notormInstance;
    }

    /**
     * 返回pdo句柄
     * @global array $_G
     * @return \PDO
     */
    public static function Pdo()
    {
        if (!self::$pdoInstance || !self::$pdoInstance instanceof PDO) {
            self::createPdoInstance();
        }
        return self::$pdoInstance;
    }

    /**
     * redis句柄
     * @return \Redis
     */
    public static function Redis()
    {
        if (!self::$redisInstance || !self::$redisInstance instanceof Redis) {
            self::createRedisInstance(REDIS_HOST, REDIS_PORT);
        }
        return self::$redisInstance;
    }

    /**
     * Alias function close()
     */
    public static function releaseConnection()
    {
        self::$pdoInstance = self::$notormInstance = null;
    }

    /**
     * 关闭数据库链接
     * 参考：http://php.net/manual/zh/pdo.connections.php
     */
    public static function Close()
    {
        self::$pdoInstance = self::$notormInstance = null;
    }

    //todo 添加超级管理员用户的连接逻辑
}