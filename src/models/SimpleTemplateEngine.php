<?php

class SimpleTemplateEngine
{
    public function render($tplpath, array $data)
    {
        //$data = ["name"=>"aaaa","age"=>18];
        ob_start();
        extract($data);
        include $tplpath;
        $str = ob_get_clean();
        return $str;
    }

}