<?php
/**
 * 公司
 * User: Lu
 * Date: 2019/1/2
 * Time: 16:26
 */

class Company extends Model
{
    public $id,$logo,$c_name, $c_addr, $tel, $icp, $updated_at, $created_at;

    /**
     * Company constructor.
     * 构造器 初始化 创建时间和更新时间
     */
    public function __construct()
    {
        $this->updated_at = $this->created_at = time();
    }

    /**
     * 查找表里第一条数据
     * @return array|null
     *
     */
    public function findOne()
    {
        $row = DB::Connect()->company()->fetch();
        return $row ? iterator_to_array($row) : null;
    }

    /**
     * 往表里插入更新数据
     * @return mixed
     *
     */
    public function insertUpdate(){
        $data = array_filter(get_object_vars($this),function($val){
            return !is_null($val);
        });
        return DB::Connect()->company()->insert_update(array('id'=>$this->id),$data,$data);
    }
}