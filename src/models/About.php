<?php
/**
 * Created by PhpStorm.
 * User: Lu
 * Date: 2019/1/8
 * Time: 10:22
 */

class About extends Model
{

    //id,内容,创建者ID,更新时间,创建时间
    public $id,$content,$user_id,$updated_at,$created_at;

    /**
     * RecruitType constructor.
     * 构造器
     */
    public function __construct()
    {
        $this->updated_at = $this->created_at = time();
    }

    /**
     * 查找表里第一条数据
     * @return array|null
     *
     */
    public function findOne()
    {
        $row = DB::Connect()->about()->fetch();
        return $row ? iterator_to_array($row) : null;
    }


    /**
     * 往表里插入更新数据
     * @return mixed
     *
     */
    public function insertUpdate(){
        $data = array_filter(get_object_vars($this),function($val){
            return !is_null($val);
        });
        return DB::Connect()->about()->insert_update(array('id'=>$this->id),$data,$data);
    }
}