<?php
/**
 * 邮件
 * User: Lu
 * Date: 2019/1/4
 * Time: 10:34
 */
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;



class Mail
{
    /**
     * 邮件初始化
     * @return PHPMailer
     * @throws Exception
     */
    public function init_mailer()
    {
        date_default_timezone_set("Asia/Shanghai");
        $mail = new PHPMailer(true);                            // Passing `true` enables exceptions

        $emailServiceConfig = new EmailServiceConfig();
        $config = $emailServiceConfig->findOne();
        $mail->Charset='UTF-8';
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $config['smtp_server'];  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $config['email'];                 // SMTP username
        $mail->Password = $config['password'];                           // SMTP password
        //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $config['port'];                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($config['email'], $config['from_name']);

        return $mail;
    }

    /**
     * 邮件发送
     * @param $mail
     * @param $toAddr
     * @param $toAddrName
     * @param string $subject
     * @param string $body
     * @param null $altBody
     * @return true
     */

    public function sendMail($mail,$toAddr,$toAddrName,$subject='主题',$body='正文',$altBody=null){
        if(is_array($toAddr)){
            foreach ($toAddr as $item){
                $mail->addAddress($item, $toAddrName);
            }
        }
        else{
            $mail->addAddress($toAddr, $toAddrName);
        }
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AltBody = $altBody;
        return $mail->send();
    }


}