<?php
/**
 * 邮件服务配置
 * User: Lu
 * Date: 2019/1/2
 * Time: 17:50
 */

class EmailServiceConfig
{
    public $id,$email,$from_name, $smtp_server, $port, $password, $updated_at, $created_at;

    public function __construct()
    {
        $this->updated_at = $this->created_at = time();
    }

    /**
     * 找到第一条数据
     * @return array|null
     *
     */
    public function findOne()
    {
        $row = DB::Connect()->email_service_config()->fetch();
        return $row ? iterator_to_array($row) : null;
    }

    /**
     * 插入更新
     * @return mixed
     *
     */
    public function insertUpdate()
    {
        $data = array_filter(get_object_vars($this), function ($val) {
            return !is_null($val);
        });
        return DB::Connect()->email_service_config()->insert_update(array('id' => $this->id), $data, $data);
    }
}