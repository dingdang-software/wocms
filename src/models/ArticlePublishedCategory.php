<?php
/**
 * Created by PhpStorm.
 * User: Lu
 * Date: 2018/12/29
 * Time: 11:45
 */

class ArticlePublishedCategory extends Model
{


    public $category_id,$category_name,$article_id;


    /**
     * 添加记录
     * @return int|object
     */
    public function addNewRecord()
    {
        //过滤掉属性为null的属性
        $data = array_filter(get_object_vars($this),function($val){
            return !is_null($val);
        });
        //插入记录
        $row = DB::Connect()->article_published_category()->insert($data);
        return $row ? $row: 0;
    }

    public function fetchAllWithArticleId()
    {
       return $this->fetchAllBy(["article_id"=>$this->article_id]);
    }

    public function deleteByArticleIdAndCategoryId(){
        return DB::Connect()->article_published_category()->where(["category_id"=>$this->category_id])->where(["article_id"=>$this->article_id])->delete();
    }


    public function insertUpdateByArticleIdAndCategoryId(){
        $data = array_filter(get_object_vars($this),function($val){
            return !is_null($val);
        });
        return DB::Connect()->article_published_category()->insert_update(array('category_id'=>$this->category_id,'article_id'=>$this->article_id),$data,$data);
    }
}