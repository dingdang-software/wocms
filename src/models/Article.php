<?php
/**
 * 文章
 * User: Lulizhi
 * Date: 2018/12/28
 * Time: 9:03
 */

class Article extends Model
{
    public $id,$title,$sub_title,$title_image,$abstract,$content,$page_view,$user_id,$updated_at,$created_at;

    /**
     * RecruitType constructor.
     * 构造器
     */
    public function __construct()
    {
        $this->updated_at = $this->created_at = time();
    }



    /**
     * 通过文章分类ID查找ID下所有的文章详情
     * @param int $category_id
     * @param int $page
     * @param int $limit
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function fetchAllByCategoryID($category_id=0,$page=1,$limit=20,$sort = "id",$order="desc"){

        $offset = ($page - 1) * $limit;
        if (!$category_id) {
            $statement='SELECT * FROM  article'.' LIMIT '.$offset.','.$limit;

            $countStatement='SELECT  COUNT(*) FROM  article';
        } else {
            $statement='SELECT * FROM  article_published_category INNER JOIN article ON article_published_category.article_id=article.id 
                    WHERE article_published_category.category_id='.$category_id.' LIMIT '.$offset.','.$limit;

            $countStatement='SELECT  COUNT(*) FROM  article_published_category INNER JOIN article ON article_published_category.article_id=article.id 
                    WHERE article_published_category.category_id='.$category_id;
        }

        $total=DB::Pdo()->query($countStatement)->fetchall(PDO::FETCH_ASSOC);
        $total=$total[0]["COUNT(*)"];
        $isEnd = ($total - $limit) <= $offset;
        if (!$total || $total >= $offset) {
            $data = DB::Pdo()->query($statement)->fetchall(PDO::FETCH_ASSOC);
        }
        $totalPage = ceil($total / $limit);
        return ["total" => $total, "is_end" => $isEnd, "total_page" => $totalPage, "rows" => $data];
    }
    public function updatePageView(){
        $this->page_view=$this->page_view+1;
        DB::Connect()->article()->where('id', $this->id)->update(['page_view'=>$this->page_view]);
    }



}