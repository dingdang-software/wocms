<?php
/**
 * Session
 * User: Lulizhi
 * Date: 2018\12\6 0006
 * Time: 8:58
 */


class Session extends Model
{
    public $id, $userinfo, $expired;


    public function __construct()
    {
        $this->expired = time() + 60 * 30;
        $this->id = Random::genStr(32, true);
    }

//    /**
//     * 添加一条session
//     * @return boolean|string
//     */
//
//    public function addOne()
//    {
//
//        $insert_arr = array('session_id'=>$this->session_id,
//            'userinfo'=>$this->userinfo,
//            'expired'=>$this->expired);
//        $result=DB::Connect()->session()->insert($insert_arr);
//
//        if($result){
//            return $this->session_id;
//        }
//        else{
//            return false;
//        }
//
//    }

    /**
     * 查找session是否匹配
     * @param $uid
     * @return bool
     */

    public function fetchOneBySessionId($uid)
    {
        $result = $this->fetchAllBy(["id" => $this->id]);

        $userinfo = json_decode($result[0]["userinfo"]);
        if ($userinfo) {
            if (($userinfo[0]->id == $uid) && ($result[0]["expired"] > time())) {
                $insert_arr = array('expired' => $this->expired);
                $re = DB::Connect()->session()->where('id', $result[0]["id"]);
                $result1 = $re->update($insert_arr);

                return true;
                //return $result1?$result[0]["id"]:false;
            }
        } else {
            return false;
        }

    }

    /**
     * 清除无效session
     */
    public function clearExpiredSession()
    {
        $re = DB::Connect()->session()->where("expired < ?", time());
        return $re->delete();
    }

}