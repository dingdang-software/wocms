<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

use Slim\Http\Request;
use Slim\Http\Response;

$app->add(function (Request $request, Response $response, $next) {

    //全局日志对象,任何地方都可以使用
    //声明才能使用全局日志对象
    global $gLogger;

    $url = $request->getUri(); //路径对象
    $sessId = $request->getHeaderLine("sess_id"); //后台会话id
    $uid = filter_var($request->getParam("uid"), FILTER_VALIDATE_INT); //uid
    //日志对象
    $gLogger = $this->logger;

    //拦截所有的后台数据请求，允许其他的或者部分后台请求通过
    if (!preg_match("/^\/bms\/((?!login|security-code|password\/reset|image\/upload|company\/logo).)/", $url->getPath())) {
        return $next($request, $response);

    }

    $session = new Session();
    $session->id = $sessId;

    if ($session->fetchOneBySessionId($uid)) {
        return $next($request, $response);
    }
    return $response->withJson(["success" => false, "msg" => "invalid"], null, JSON_UNESCAPED_UNICODE);
});

