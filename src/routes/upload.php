<?php
/**
 * 上传
 * User: Lu lizhi
 * Date: 2018\12\24 0024
 * Time: 17:32
 */

use Slim\Http\Request;
use Slim\Http\Response;

//图片上传api
$app->post('/bms/image/upload', function (Request $request, Response $response, array $args) {
    $fileHandle = $request->getUploadedFiles()['image'];
    if (!$fileHandle) {
        return $response->withJson(["success" => false, "msg" => "nodata"], null, JSON_UNESCAPED_UNICODE);
    }

    $type = $fileHandle->getClientMediaType();
    $size = $fileHandle->getSize();
    $errorCode = $fileHandle->getError();

    if ($errorCode) {
        return $response->withJson(["success" => false, "msg" => "error", "data" => $errorCode], null, JSON_UNESCAPED_UNICODE);
    }

    if (!in_array($type, ['image/png', 'image/jpeg'])) {
        return $response->withJson(["success" => false, "msg" => "typeError"], null, JSON_UNESCAPED_UNICODE);
    }

    if (($size > 1024 * 1024 * 4)) {
        return $response->withJson(["success" => false, "msg" => "dataLarge"], null, JSON_UNESCAPED_UNICODE);
    }

    $random = Random::genStr(24, true);
    $fileHandle->moveTo('public/upload/' . $random . '.jpg');
    $uri = 'upload/' . $random . '.jpg';
    $name = $random . '.jpg';

    $fileUpload = new FileUpload();
    $fileUpload->name = $name;
    $fileUpload->uri = $uri;
    $fileUpload->type = $type;
    $fileUpload->size = $size;

    $result = $fileUpload->addNewRecord();
    if (!$result) {
        return $response->withJson(["success" => false, "msg" => "failToInsertTable"], null, JSON_UNESCAPED_UNICODE);
    }
    $data['id'] = $result;
    $data['uri'] = $uri;

    return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
});

//获取上传文件列表
$app->get("/bms/upload/list", function (Request $request, Response $response, array $args) {


    $page = filter_var($request->getParam("page", 1), FILTER_VALIDATE_INT);
    $order = $request->getParam("order", 'desc');
    $sortBy = $request->getParam("sortBy", 'id');
    $limit = filter_var($request->getParam("limit", 10), FILTER_VALIDATE_INT);

    if (!$page || $page < 1) {
        $page = 1;
    }
    if (!in_array($order, ['desc', 'asc'])) {
        $order = 'desc';
    }
    $fileUpload = new FileUpload();

    $data = $fileUpload->fetchList($page, $limit, $sortBy, $order);


    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }

});

//删除上传记录
$app->post('/bms/upload/delete', function (Request $request, Response $response, array $args) {

    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);

    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $fileUpload = new FileUpload();
    $fileUpload->id = $id;
    $data = $fileUpload->deleteDataAndFileById();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});