<?php
/**
 * 主页路由
 * User: ALU
 * Date: 2018/12/14
 * Time: 10:20
 */

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
//官网首页
$app->get('/', function (Request $request, Response $response, array $args) {

    $nav= new Nav();
    $navList = $nav->fetchAllCanShow();

    $company = new Company();
    $companyData=$company->findOne();

    $slide = new Slide();
    $slideList = $slide->fetchAllCanShow();

    $footer = new Footer();
    $footerData = $footer->fetchFooters();

    $blockShowArticleCategory = new BlockShowArticleCategory();
    $blockShowArticleCategoryData = $blockShowArticleCategory->fetchAll();

    $data['blockData'] = $blockShowArticleCategoryData;
    $data['navList'] = $navList;
    $data['logo'] = $companyData['logo'];
    $data['slideList'] = $slideList;
    $data['footer2List'] = $footerData['footer2List'];
    $data['footer1List'] = $footerData['footer1List'];
    return $this->renderer->render($response, 'index.phtml', $data);
});
