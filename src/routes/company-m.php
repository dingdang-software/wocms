<?php
/**
 * 公司信息
 * User: Lu
 * Date: 2019/1/2
 * Time: 16:23
 */
use Slim\Http\Request;
use Slim\Http\Response;

//获取公司logo
$app->get("/bms/company/logo", function (Request $request, Response $response, array $args) {


    $company = new Company();

    $data=$company->findOne();

    if($data){
        $companyData['logo']=$data['logo'];
        $companyData['c_name']=$data['c_name'];
    }

    if ($companyData){
        return $response->withJson(["success"=>true,"msg"=>"成功",'data'=>$companyData],null,JSON_UNESCAPED_UNICODE);
    }else{
        return $response->withJson(["success"=>false,"msg"=>"失败"],null,JSON_UNESCAPED_UNICODE);
    }

});

//获取公司信息
$app->get("/bms/company", function (Request $request, Response $response, array $args) {


    $company = new Company();

    $data=$company->findOne();


    if ($data){
        return $response->withJson(["success"=>true,"msg"=>"成功",'data'=>$data],null,JSON_UNESCAPED_UNICODE);
    }else{
        return $response->withJson(["success"=>false,"msg"=>"失败"],null,JSON_UNESCAPED_UNICODE);
    }

});


//更新公司信息
$app->post("/bms/company/update", function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id" ),FILTER_VALIDATE_INT);
    $logo=$request->getParam("logo");
    $c_name=$request->getParam("c_name");
    $c_addr=$request->getParam("c_addr");
    $tel=$request->getParam("tel");
    $icp=$request->getParam("icp");

    if(!$id||!$logo||!$c_name){
        return $response->withJson(["success"=>false,"msg"=>"invalid"],null,JSON_UNESCAPED_UNICODE);
    }



    $company = new Company();
    $company->id=$id;
    $company->logo=$logo;
    $company->c_name=$c_name;
    $company->c_addr=$c_addr;
    $company->tel=$tel;
    $company->icp=$icp;
    $data=$company->insertUpdate();


    if ($data){
        return $response->withJson(["success"=>true,"msg"=>"成功",'data'=>$data],null,JSON_UNESCAPED_UNICODE);
    }else{
        return $response->withJson(["success"=>false,"msg"=>"失败"],null,JSON_UNESCAPED_UNICODE);
    }

});
