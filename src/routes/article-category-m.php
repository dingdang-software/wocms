<?php
/**
 * bms-文章类别管理
 * User: Lu lizhi
 * Date: 2018/12/28
 * Time: 17:55
 */

use Slim\Http\Request;
use Slim\Http\Response;

//获取文章类别列表，分页
$app->get("/bms/article-category/list", function (Request $request, Response $response, array $args) {

    $page = filter_var($request->getParam("page", 1), FILTER_VALIDATE_INT);
    $order = $request->getParam("order", 'desc');
    $sortBy = $request->getParam("sortBy", 'id');
    $limit = filter_var($request->getParam("limit", 10), FILTER_VALIDATE_INT);



    if (!$page || $page < 1) {
        $page = 1;
    }
    if (!in_array($order, ['desc', 'asc'])) {
        $order = 'desc';
    }
    $articleCategory = new ArticleCategory();

    $data = $articleCategory->fetchList($page, $limit, $sortBy, $order);


    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }

});

//获取文章所有类别，用户编辑文章时使用
$app->get("/bms/article-category", function (Request $request, Response $response, array $args) {




    $articleCategory = new ArticleCategory();

    $data = $articleCategory->fetchAll();


    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }

});

//更新文章类别
$app->post('/bms/article-category/update', function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);
    $title = $request->getParam("title");

    if ( !$title || !$id) {
        return $response->withJson(["success" => false, "msg" => "参数错误"], null, JSON_UNESCAPED_UNICODE);
    }


    $articleCategory = new ArticleCategory();
    $articleCategory->id = $id;
    $articleCategory->title = $title;


    $data = $articleCategory->updateBy();
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//添加文章类别
$app->post('/bms/article-category/add', function (Request $request, Response $response, array $args) {


    $title = $request->getParam("title");

    if (!$title) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }




    $articleCategory = new ArticleCategory();
    $articleCategory->title = $title;
    $articleCategory->user_id = $uid;

    $data = $articleCategory->addNewRecord();
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//删除文章类别
$app->post('/bms/article-category/delete', function (Request $request, Response $response, array $args) {

    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);

    if ($id) {
        return $response->withJson(["success" => false, "msg" => "参数错误"], null, JSON_UNESCAPED_UNICODE);
    }

    $articleCategory = new ArticleCategory();
    $articleCategory->id = $id;
    $data = $articleCategory->deleteById();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});