<?php
/**
 * bms-邮件配置
 * User: Lu
 * Date: 2019/1/2
 * Time: 17:42
 */
use Slim\Http\Request;
use Slim\Http\Response;

//获得邮件配置信息
$app->get("/bms/email-service-config", function (Request $request, Response $response, array $args) {


    $emailServiceConfig = new EmailServiceConfig();

    $data=$emailServiceConfig->findOne();


    if ($data){
        return $response->withJson(["success"=>true,"msg"=>"成功",'data'=>$data],null,JSON_UNESCAPED_UNICODE);
    }else{
        return $response->withJson(["success"=>false,"msg"=>"失败"],null,JSON_UNESCAPED_UNICODE);
    }

});

//更新邮件配置信息
$app->post("/bms/email-service-config/update", function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id" ),FILTER_VALIDATE_INT);

    $email=$request->getParam("email");
    $from_name=$request->getParam("from_name");
    $smtp_server=$request->getParam("smtp_server");
    $port=$request->getParam("port");
    $password=$request->getParam("password");
    $to_email=$request->getParam("to_email");
    if(!$email||!$from_name||!$smtp_server||!$port||!$password||!$to_email){
        return $response->withJson(["success"=>false,"msg"=>"有数据为空"],null,JSON_UNESCAPED_UNICODE);
    }



    $emailServiceConfig = new EmailServiceConfig();
    $emailServiceConfig->id=$id;
    $emailServiceConfig->email=$email;
    $emailServiceConfig->to_email=$to_email;
    $emailServiceConfig->from_name=$from_name;
    $emailServiceConfig->smtp_server=$smtp_server;
    $emailServiceConfig->port=$port;
    $emailServiceConfig->password=$password;
    $data=$emailServiceConfig->insertUpdate();


    if ($data){
        return $response->withJson(["success"=>true,"msg"=>"成功",'data'=>$data],null,JSON_UNESCAPED_UNICODE);
    }else{
        return $response->withJson(["success"=>false,"msg"=>"失败"],null,JSON_UNESCAPED_UNICODE);
    }

});
