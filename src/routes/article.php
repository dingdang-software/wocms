<?php
/**
 * 文章
 * User: Lu lizhi
 * Date: 2018\12\24 0024
 * Time: 16:43
 */

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * 官网获取文章
 */

$app->get("/article", function (Request $request, Response $response, array $args) {

    $id = filter_var($request->getParam("id", 1), FILTER_VALIDATE_INT);

    $article = new Article();
    $article->id=$id;
    $articleResult = $article->findOneById();
    if($articleResult){
        $article->page_view=$articleResult['page_view'];
        $article->updatePageView();
    }

    $nav= new Nav();
    $navList = $nav->fetchAllCanShow();

    $footer = new Footer();
    $footerData = $footer->fetchFooters();

    $company = new Company();
    $companyData=$company->findOne();
    $data['logo'] = $companyData['logo'];
    $data['navList'] = $navList;
    $data['footer2List'] = $footerData['footer2List'];
    $data['footer1List'] = $footerData['footer1List'];

    $data["title"] = $articleResult["title"];
    $data["sub_title"] = $articleResult["sub_title"];
    $data["abstract"] = $articleResult["abstract"];
    $data["content"] = $articleResult["content"];
    $data["page_view"] = $articleResult["page_view"];
    $data["updated_at"] = date("Y-m-d H:i:s",$articleResult["updated_at"]);
    //var_dump($data["jobs"]);
    return $this->renderer->render($response, 'article.phtml', $data);

});

/**
 * 官网根据分类获取文章列表
 */
$app->get("/article/list", function (Request $request, Response $response, array $args) {

    $category_id = filter_var($request->getParam("category_id", 0), FILTER_VALIDATE_INT);
    $page = filter_var($request->getParam("page", 1), FILTER_VALIDATE_INT);$order = $request->getParam("order", 'desc');
    $limit = 5;
    if (!$page || $page < 1) {
        $page = 1;
    }
    $article = new Article();
    $articleListResult = $article->fetchAllByCategoryID($category_id,$page,$limit);

    $nav= new Nav();
    $navList = $nav->fetchAllCanShow();

    $footer = new Footer();
    $footerData = $footer->fetchFooters();

    $articleCategory = new ArticleCategory();
    $categoryList=$articleCategory->fetchAll();

    $company = new Company();
    $companyData=$company->findOne();
    $data['logo'] = $companyData['logo'];
    $data['navList'] = $navList;
    $data['footer2List'] = $footerData['footer2List'];
    $data['footer1List'] = $footerData['footer1List'];
    $data['categoryList'] = $categoryList;
    $data['categoryId'] = $category_id;

    $data['categoryName']=$categoryList[$category_id-1]['title'];
    $data['page']= $page;
    $data['category_id'] = $category_id;
    $data["total"] = $articleListResult["total"];
    $data["total_page"] = $articleListResult["total_page"];
    $data["is_end"] = $articleListResult["is_end"];
    $data["articleList"] = $articleListResult["rows"];
    //var_dump($data["jobs"]);
    return $this->renderer->render($response, 'article-list.phtml', $data);

});








