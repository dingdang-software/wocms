<?php
/**
 * bms-招聘分类路由
 * User: Lu
 * Date: 2018/12/26
 * Time: 11:36
 */


use Slim\Http\Request;
use Slim\Http\Response;

//获得全部招聘分类信息
$app->get("/bms/recruit-types", function (Request $request, Response $response, array $args) {


    $recruitTypes = new RecruitTypes();

    $data = $recruitTypes->fetchAll();


    return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);

});

//获得招聘分类信息列表
$app->get("/bms/recruit-types/list", function (Request $request, Response $response, array $args) {


    $page = filter_var($request->getParam("page", 1), FILTER_VALIDATE_INT);
    $order = $request->getParam("order", 'desc');
    $sortBy = $request->getParam("sortBy", 'id');
    $limit = filter_var($request->getParam("limit", 10), FILTER_VALIDATE_INT);


    if (!$page || $page < 1) {
        $page = 1;
    }
    if (!in_array($order, ['desc', 'asc'])) {
        $order = 'desc';
    }
    $recruitTypes = new RecruitTypes();

    $data = $recruitTypes->fetchList($page, $limit, $sortBy, $order);


    return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);

});

//更新招聘分类记录
$app->post('/bms/recruit-types/update', function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);
    $o_type = $request->getParam("o_type");
    $cnt = filter_var($request->getParam("cnt"), FILTER_VALIDATE_INT);

    if (!$o_type || !$cnt || !$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $recruitTypes = new RecruitTypes();
    $recruitTypes->id = $id;
    $recruitTypes->o_type = $o_type;
    $recruitTypes->cnt = $cnt;


    $data = $recruitTypes->updateBy();
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//添加招聘分类
$app->post('/bms/recruit-types/add', function (Request $request, Response $response, array $args) {


    $o_type = $request->getParam("o_type");
    $cnt = filter_var($request->getParam("cnt"), FILTER_VALIDATE_INT);

    if (!$o_type || !$cnt) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $recruitTypes = new RecruitTypes();
    $recruitTypes->o_type = $o_type;
    $recruitTypes->cnt = $cnt;


    $data = $recruitTypes->addNewRecord();
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//删除招聘分类
$app->post('/bms/recruit-types/delete', function (Request $request, Response $response, array $args) {

    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);

    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $recruitTypes = new RecruitTypes();
    $recruitTypes->id = $id;
    $data = $recruitTypes->deleteById();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});