<?php
/**
 * 后台管理 首页
 * User: Lu
 * Date: 2019/1/9
 * Time: 11:25
 */

use Slim\Http\Request;
use Slim\Http\Response;

//后台管理首页
$app->get("/bms/home", function (Request $request, Response $response, array $args) {


    $article = new Article();
    $articleNum = $article->countAll();

    $user = new User();
    $userNum = $user->countAll();

    $recruit = new Recruit();
    $recruitNum = $recruit->countAll();

    $fileUpload = new FileUpload();
    $fileUploadNum = $fileUpload->countAll();

    $slide = new Slide();
    $slideNum = $slide->countAll();

    $consult = new Consult();
    $consultNum = $consult->countAll();

    $session = new Session();
    $sessionNum = $session->countAll();

    $osVersion = PHP_OS;
    $phpVersion = PHP_VERSION;
    $timezone = function_exists('data_default_time_zone_get') ? data_default_time_zone_get() : 'no_timezone';
    $webServer = $_SERVER['SERVER_SOFTWARE'];
    $ip = $_SERVER['SERVER_NAME'];


    $data = [
        "articleNum" => $articleNum,
        "userNum" => $userNum,
        "recruitNum" => $recruitNum,
        "fileUploadNum" => $fileUploadNum,
        "slideNum" => $slideNum,
        "consultNum" => $consultNum,
        "sessionNum" => $sessionNum,
        "phpVersion" => $phpVersion,
        "osVersion" => $osVersion,
        "timezone" => $timezone,
        "webServer" => $webServer,
        "ip" => $ip,
    ];


    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//后台管理首页
$app->get("/bms/session/clear", function (Request $request, Response $response, array $args) {

    $session = new Session();
    $data = $session->clearExpiredSession();
    return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);

});