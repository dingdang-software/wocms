<?php
/**
 * bms-关于我们.
 * User: Lu
 * Date: 2019/2/21
 * Time: 9:14
 */

use Slim\Http\Request;
use Slim\Http\Response;

//获得关于我们的信息
$app->get("/bms/about", function (Request $request, Response $response, array $args) {


    $about = new About();

    $data = $about->findOne();


    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }

});

//更新关于我们
$app->post("/bms/about/update", function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id", 1), FILTER_VALIDATE_INT);

    $content = $request->getParam("content");
    if (!$content || !$id) {
        return $response->withJson(["success" => false, "msg" => "参数错误"], null, JSON_UNESCAPED_UNICODE);
    }


    $about = new About();
    $about->id = $id;
    $about->content = $content;
    $data = $about->insertUpdate();


    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }

});