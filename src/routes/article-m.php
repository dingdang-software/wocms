<?php
/**
 * bms-文章管理api
 * User: Lu
 * Date: 2019/2/21
 * Time: 9:17
 */

use Slim\Http\Request;
use Slim\Http\Response;

//获取文章列表
$app->get("/bms/article/list", function (Request $request, Response $response, array $args) {


    $page = filter_var($request->getParam("page", 1), FILTER_VALIDATE_INT);
    $order = $request->getParam("order", 'desc');
    $sortBy = $request->getParam("sortBy", 'id');
    $limit = filter_var($request->getParam("limit", 10), FILTER_VALIDATE_INT);

    if (!$page || $page < 1) {
        $page = 1;
    }
    if (!in_array($order, ['desc', 'asc'])) {
        $order = 'desc';
    }
    $article = new Article();

    $data = $article->fetchList($page, $limit, $sortBy, $order);
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//获取单个文章详情
$app->get("/bms/article/detail", function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);
    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }

    $article = new Article();
    $article->id = $id;
    $data = $article->findOneById();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//文章添加
$app->post("/bms/article/add", function (Request $request, Response $response, array $args) {


    $title = $request->getParam("title");
    $sub_title = $request->getParam("sub_title");
    $title_image = $request->getParam("title_image");
    $abstract = $request->getParam("abstract");
    $content = $request->getParam("content");


    if (!$uid || !$title || !$content) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $article = new Article();
    $article->title = $title;
    $article->sub_title = $sub_title;
    $article->title_image = $title_image;
    $article->abstract = $abstract;
    $article->content = $content;
    $article->user_id = $uid;
    $data = $article->addNewRecord();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//文章删除
$app->post('/bms/article/delete', function (Request $request, Response $response, array $args) {

    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);

    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }

    $article = new Article();
    $article->id = $id;
    $data = $article->deleteById();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//文章更新
$app->post('/bms/article/update', function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);
    $title = $request->getParam("title");
    $sub_title = $request->getParam("sub_title");
    $title_image = $request->getParam("title_image");
    $abstract = $request->getParam("abstract");
    $content = $request->getParam("content");

    if (!$title || !$content || !$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $article = new Article();
    $article->id = $id;
    $article->title = $title;
    $article->sub_title = $sub_title;
    $article->title_image = $title_image;
    $article->abstract = $abstract;
    $article->content = $content;


    $data = $article->updateBy();
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});
