<?php
/**
 * 招聘页面
 * User: Lu
 * Date: 2019/2/21
 * Time: 16:29
 */

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * 官网获取能显示的招聘信息
 */

$app->get("/recruits", function (Request $request, Response $response, array $args) {

    $page = filter_var($request->getParam("page", 1), FILTER_VALIDATE_INT);
    $order = $request->getParam("order", 'desc');
    $recruit_type_id = filter_var($request->getParam("recruit_type_id", 0), FILTER_VALIDATE_INT);
    $limit = 3;
    if (!$page || $page < 1) {
        $page = 1;
    }

    $recruitTypes = new RecruitTypes();
    $recruitTypesResult = $recruitTypes->fetchList();

    $recruit = new Recruit();
    $recruit->recruit_type_id = $recruit_type_id;
    $recruitResult = $recruit->fetchList($page, $limit);

    $nav = new Nav();
    $navList = $nav->fetchAllCanShow();

    $footer = new Footer();
    $footerData = $footer->fetchFooters();
    $company = new Company();
    $companyData = $company->findOne();
    $data['logo'] = $companyData['logo'];
    $data['navList'] = $navList;
    $data['footer2List'] = $footerData['footer2List'];
    $data['footer1List'] = $footerData['footer1List'];
    $data["total"] = $recruitResult["total"];
    $data["total_page"] = $recruitResult["total_page"];
    $data["is_end"] = $recruitResult["is_end"];
    $data["jobs"] = $recruitResult["rows"];
    $data["recruitTypesList"] = $recruitTypesResult;
    $data["recruit_type_id"] = $recruit_type_id;
    $data["page"] = $page;
    //var_dump($data["jobs"]);
    return $this->renderer->render($response, 'recruit.phtml', $data);

});