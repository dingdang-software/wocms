<?php
/**
 * bms-咨询api
 * User: Lu
 * Date: 2019/2/21
 * Time: 15:59
 */

use Slim\Http\Request;
use Slim\Http\Response;

//获得咨询列表
$app->get("/bms/consult/list", function (Request $request, Response $response, array $args) {



    $page = filter_var($request->getParam("page", 1), FILTER_VALIDATE_INT);
    $order = $request->getParam("order", 'desc');
    $sortBy= $request->getParam("sortBy", 'id');
    $limit = filter_var($request->getParam("limit", 10), FILTER_VALIDATE_INT);

    if (!$page || $page < 1) {
        $page = 1;
    }
    if(!in_array($order,['desc','asc'])){
        $order='desc';
    }
    $consult = new Consult();

    $data = $consult->fetchList($page, $limit, $sortBy,$order);


    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }

});


//删除咨询记录
$app->post('/bms/consult/delete', function (Request $request, Response $response, array $args) {

    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);

    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $consult = new Consult();
    $consult->id = $id;
    $data = $consult->deleteById();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});
