<?php
/**
 * 账户相关api
 * User: Lu lizhi
 * Date: 2018\12\20 0020
 * Time: 14:55
 */

use Slim\Http\Request;
use Slim\Http\Response;


//后台登录api
$app->post('/bms/login', function (Request $request, Response $response, array $args) {

    $account = $request->getParam("account" );
    $password = $request->getParam("password");
    if(!$account||!$password){
        return $response->withJson(["success"=>false,"msg"=>"账号或者密码为空"],null,JSON_UNESCAPED_UNICODE);
    }

    $user= new User();
    $user->password=md5("posd" . $password);

    $data=$user->fetchOneCanLogin($account);
    if($data){
        $session=new Session();
        $session->userinfo=json_encode($data);
        $result=$session->addNewRecord();
    }
    else{
        $result=null;
    }

    if ($result){
        return $response->withJson(["success"=>true,"msg"=>"成功",'data'=>$data,'session_id'=>$result],null,JSON_UNESCAPED_UNICODE);
    }else{
        return $response->withJson(["success"=>false,"msg"=>"失败"],null,JSON_UNESCAPED_UNICODE);
    }
});


//后台获取验证码api
$app->post('/bms/security-code', function (Request $request, Response $response, array $args) {

    $email = $request->getParam("email" );
    if(!$email){
        return $response->withJson(["success"=>false,"msg"=>"邮箱为空"],null,JSON_UNESCAPED_UNICODE);
    }
    $user= new User();
    $user->email=$email;
    $code=$user->sendSecurityCodeToEmail();
    $data=false;
    if($code){
        $securityCode= new SecurityCode();
        $securityCode->security_code=$code;
        $securityCode->email=$email;
        $data=$securityCode->addNewRecord();
    }

    if ($data){
        return $response->withJson(["success"=>true,"msg"=>"成功"],null,JSON_UNESCAPED_UNICODE);
    }else{
        return $response->withJson(["success"=>false,"msg"=>"失败"],null,JSON_UNESCAPED_UNICODE);
    }
});

//后台重置密码api
$app->post('/bms/password/reset', function (Request $request, Response $response, array $args) {

    $email = $request->getParam("email" );
    $securityCode= $request->getParam("securityCode" );
    $password = $request->getParam("password" );
    if(!$email||!$securityCode||!$password){
        return $response->withJson(["success"=>false,"msg"=>"nodata"],null,JSON_UNESCAPED_UNICODE);
    }

    $security_code= new SecurityCode();
    $security_code->email=$email;
    $security_code->security_code=$securityCode;
    $hasData=$security_code->fetchOneByEmailAndSecurityCode();
    $data=0;
    if ($hasData){
        $user= new User();
        $user->email=$email;
        $user->password=md5("posd" . $password);;
        $data=$user->updateBy("email",$email);
    }

    if ($data){
        return $response->withJson(["success"=>true,"msg"=>"成功","data"=>$data],null,JSON_UNESCAPED_UNICODE);
    }else{
        return $response->withJson(["success"=>false,"msg"=>"验证码已失效"],null,JSON_UNESCAPED_UNICODE);
    }
});