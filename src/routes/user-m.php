<?php
/**
 * 用户
 * User: Lu lizhi
 * Date: 2018\12\22 0022
 * Time: 11:44
 */

use Slim\Http\Request;
use Slim\Http\Response;

//获得用户列表
$app->get('/bms/user/list', function (Request $request, Response $response, array $args) {


    $page = filter_var($request->getParam("page", 1), FILTER_VALIDATE_INT);
    $order = $request->getParam("order", 'desc');
    $sortBy = $request->getParam("sortBy", 'id');
    $limit = filter_var($request->getParam("limit", 10), FILTER_VALIDATE_INT);

    if (!$page || $page < 1) {
        $page = 1;
    }
    if (!in_array($order, ['desc', 'asc'])) {
        $order = 'desc';
    }

    $user = new User();
    $data = $user->fetchList($page, $limit, $sortBy, $order);


    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

$app->get('/bms/user/detail', function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);

    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "nodata"], null, JSON_UNESCAPED_UNICODE);
    }


    $user = new User();
    $user->id = $id;
    $data = $user->findOneById();


    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

$app->post('/bms/user/add', function (Request $request, Response $response, array $args) {


    $name = $request->getParam("name");
    $phone = $request->getParam("phone");
    $email = $request->getParam("email");
    $password = $request->getParam("password");
    $avatar = $request->getParam("avatar");
    $sex = filter_var($request->getParam("sex"));

    if (!$name || !$password | !$phone | !$email) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $user = new User();
    $user->name = $name;
    $user->phone = $phone;
    $user->email = $email;
    $user->password = md5("posd" . $password);
    $user->avatar = $avatar;
    $user->sex = $sex;
    $user->phone = $phone;
    $user->role_level = 1;
    $user->email = $email;

    $isRepeat = $user->fetchAllPhoneAndEMailRepeat();
    if ($isRepeat) {
        return $response->withJson(["success" => false, "msg" => "repeat"], null, JSON_UNESCAPED_UNICODE);
    }
    $data = $user->addNewRecord();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

$app->post('/bms/user/update', function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);
    $name = $request->getParam("name");
    $phone = $request->getParam("phone");
    $email = $request->getParam("email");
    $password = $request->getParam("password");
    $avatar = $request->getParam("avatar");
    $sex = filter_var($request->getParam("sex", 1), FILTER_VALIDATE_INT);

    if (!$name || !$password | !$phone | !$email || !$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $user = new User();
    $user->id = $id;
    $user->name = $name;
    $user->phone = $phone;
    $user->email = $email;
    $user->password = md5("posd" . $password);
    $user->avatar = $avatar;
    $user->sex = $sex;
    $user->phone = $phone;
    $user->email = $email;

    $isRepeat = $user->fetchAllPhoneAndEMailRepeat();
    if ($isRepeat) {
        return $response->withJson(["success" => false, "msg" => "repeat"], null, JSON_UNESCAPED_UNICODE);
    }
    $data = $user->updateBy();
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

$app->post('/bms/user/delete', function (Request $request, Response $response, array $args) {

    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);
    $uid = filter_var($request->getParam("uid"), FILTER_VALIDATE_INT); //uid
    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $user = new User();
    $user->id = $id;
    $userData = $user->findOneById();
    if ($userData['role_level'] == 0 || $uid == $id) {
        return $response->withJson(["success" => false, "msg" => "noPermission"], null, JSON_UNESCAPED_UNICODE);
    }
    $data = $user->deleteById();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});


