<?php
/**
 * 关于我们路由
 * User: Lu
 * Date: 2019/1/8
 * Time: 10:17
 */

use Slim\Http\Request;
use Slim\Http\Response;

//官网关于我们页面
$app->get("/about", function (Request $request, Response $response, array $args) {

    $nav= new Nav();
    $navList = $nav->fetchAllCanShow();

    $company = new Company();
    $companyData=$company->findOne();

    $footer = new Footer();
    $footerData = $footer->fetchFooters();

    $about = new About();

    $aboutData=$about->findOne();

    $data['content']=$aboutData['content'];
    $data['navList'] = $navList;
    $data['logo'] = $companyData['logo'];
    $data['footer2List'] = $footerData['footer2List'];
    $data['footer1List'] = $footerData['footer1List'];
    return $this->renderer->render($response, 'about.phtml', $data);

});




