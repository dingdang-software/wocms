<?php
/**
 * 咨询
 * User: Lu
 * Date: 2019/1/3
 * Time: 10:02
 */

use Slim\Http\Request;
use Slim\Http\Response;

//官网咨询页
$app->get('/consult', function (Request $request, Response $response, array $args) {

    $nav = new Nav();
    $navList = $nav->fetchAllCanShow();

    $footer = new Footer();
    $footerData = $footer->fetchFooters();
    $company = new Company();
    $companyData = $company->findOne();
    $data['logo'] = $companyData['logo'];
    $data['navList'] = $navList;
    $data['footer2List'] = $footerData['footer2List'];
    $data['footer1List'] = $footerData['footer1List'];
    return $this->renderer->render($response, 'consult.phtml', $data);
});

//接受咨询添加记录
$app->post('/consult/add', function (Request $request, Response $response, array $args) {
    $c_email = $request->getParam("c_email");
    $c_phone = $request->getParam("c_phone");
    $c_name = $request->getParam("c_name");
    $c_requirement = $request->getParam("c_requirement");

    $consult = new Consult();
    $consult->c_email = $c_email;
    $consult->c_name = $c_phone;
    $consult->c_name = $c_name;
    $consult->c_requirement = $c_requirement;
    $data = $consult->addNewRecord();

    $email_service_config = new EmailServiceConfig();
    $mailData = $email_service_config->findOne();
    $arrToEmail = explode(",", $mailData['to_email']);


    $mail = new Mail();
    $mailHandle = $mail->init_mailer();
    $body = '有用户咨询信息啦！！内容为:' . $c_requirement . '<br/>来自用户:' . $c_name . '<br/>电话为:' . $c_phone . '<br/>邮箱为:' . $c_email;
    $mail->sendMail($mailHandle, $arrToEmail, 'Admin', '用户咨询内容', $body, null, $c_email, $c_name);

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});