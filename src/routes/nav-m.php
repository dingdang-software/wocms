<?php
/**
 * bms-导航
 * User: Lu
 * Date: 2018/12/29
 * Time: 14:20
 */

use Slim\Http\Request;
use Slim\Http\Response;

//获得导航列表数据
$app->get("/bms/nav/list", function (Request $request, Response $response, array $args) {




    $page = filter_var($request->getParam("page", 1), FILTER_VALIDATE_INT);
    $order = $request->getParam("order", 'desc');
     $sortBy= $request->getParam("sortBy", 'id');
    $limit = filter_var($request->getParam("limit", 10), FILTER_VALIDATE_INT);



    if (!$page || $page < 1) {
        $page = 1;
    }
    if(!in_array($order,['desc','asc'])){
        $order='desc';
    }

    $nav = new Nav();

    $data = $nav->fetchList($page,$limit,$sortBy,$order);


    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }

});

$app->post('/bms/nav/update', function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);
    $parent_nav_id = filter_var($request->getParam("parent_nav_id"), FILTER_VALIDATE_INT);
    $title = $request->getParam("title");
    $is_show = $request->getParam("is_show");
    $level = filter_var($request->getParam("level"), FILTER_VALIDATE_INT);
    $link_url = $request->getParam("link_url");

    if ( !$title || !$id || $parent_nav_id === null || !$is_show || $level === null || !$link_url) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }




    $nav = new Nav();
    $nav->id = $id;
    $nav->parent_nav_id = $parent_nav_id;
    $nav->title = $title;
    $nav->is_show = $is_show;
    $nav->level = $level;
    $nav->link_url = $link_url;


    $data = $nav->updateBy();
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

$app->post('/bms/nav/add', function (Request $request, Response $response, array $args) {


    $parent_nav_id = filter_var($request->getParam("parent_nav_id"), FILTER_VALIDATE_INT);
    $title = $request->getParam("title");
    $is_show = $request->getParam("is_show");
    $level = filter_var($request->getParam("level"), FILTER_VALIDATE_INT);
    $link_url = $request->getParam("link_url");

    if ( !$title) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }




    $nav = new Nav();
    $nav->parent_nav_id = $parent_nav_id;
    $nav->title = $title;
    $nav->is_show = $is_show;
    $nav->level = $level;
    $nav->link_url = $link_url;

    $data = $nav->addNewRecord();
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

$app->post('/bms/nav/delete', function (Request $request, Response $response, array $args) {

    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);

    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }



    $nav = new Nav();
    $nav->id = $id;
    $data = $nav->deleteById();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});