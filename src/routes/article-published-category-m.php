<?php
/**
 * bms-文章和文章分类对应表.
 * User: Lu
 * Date: 2018/12/29
 * Time: 10:55
 */

use Slim\Http\Request;
use Slim\Http\Response;

//文章分类对应表添加记录
$app->post('/bms/article-published-category/add', function (Request $request, Response $response, array $args) {


    $arrayCategory = $request->getParam("arrayCategory");
    $article_id = filter_var($request->getParam("article_id"), FILTER_VALIDATE_INT);

    if (!$arrayCategory || !$article_id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $articlePublishedCategory = new ArticlePublishedCategory();
    $articlePublishedCategory->article_id = $article_id;
    foreach ($arrayCategory as $item) {
        $row = json_decode($item);
        $articlePublishedCategory->category_id = $row->id;
        $articlePublishedCategory->category_name = $row->title;
        $data[] = $articlePublishedCategory->addNewRecord();
    }

    foreach ($data as $row) {
        if (!$row) {
            return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
        }
    }
    return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);

});

//文章分类对应表更新记录
$app->post('/bms/article-published-category/update', function (Request $request, Response $response, array $args) {


    $arrayCategory = $request->getParam("arrayCategory");
    $article_id = filter_var($request->getParam("article_id"), FILTER_VALIDATE_INT);

    if (!$arrayCategory || !$article_id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $articlePublishedCategory = new ArticlePublishedCategory();
    $articlePublishedCategory->article_id = $article_id;
    $articleArray = $articlePublishedCategory->fetchAllWithArticleId();

    foreach ($articleArray as $item) {
        $breakOuter = false;
        foreach ($arrayCategory as $item2) {

            $row = json_decode($item2);
            if ($row->id == $item['category_id']) {
                $breakOuter = true;
                break;
            }
        }
        if (!$breakOuter) {
            $articlePublishedCategory->article_id = $item['article_id'];
            $articlePublishedCategory->category_id = $item['category_id'];
            $articlePublishedCategory->deleteByArticleIdAndCategoryId();
        }

    }


    $articlePublishedCategory->article_id = $article_id;
    foreach ($arrayCategory as $item) {
        $row = json_decode($item);
        $articlePublishedCategory->category_name = $row->title;
        $articlePublishedCategory->category_id = $row->id;
        $data[] = $articlePublishedCategory->insertUpdateByArticleIdAndCategoryId();
    }

    foreach ($data as $row) {
        if ($row) {
            return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
        }

        return $response->withJson(["success" => false, "msg" => "数据未更新"], null, JSON_UNESCAPED_UNICODE);
    }


});


//文章分类对应表单挑记录信息
$app->get('/bms/article-published-category/detail', function (Request $request, Response $response, array $args) {

    $article_id = filter_var($request->getParam("article_id"), FILTER_VALIDATE_INT);

    if (!$article_id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $articlePublishedCategory = new ArticlePublishedCategory();
    $articlePublishedCategory->article_id = $article_id;
    $data = $articlePublishedCategory->fetchAllWithArticleId();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});