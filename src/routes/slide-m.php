<?php
/**
 * bms-幻灯片
 * User: Lu lizhi
 * Date: 2018\12\24 0024
 * Time: 14:17
 */

use Slim\Http\Request;
use Slim\Http\Response;

//添加幻灯片
$app->post('/bms/slide/add', function (Request $request, Response $response, array $args) {

    $uid = filter_var($request->getParam("uid"), FILTER_VALIDATE_INT); //uid
    $image = $request->getParam("image");
    $title = $request->getParam("title");
    $link_url = $request->getParam("link_url");
    $remark = $request->getParam("remark");
    $weight = filter_var($request->getParam("weight"), FILTER_VALIDATE_INT);
    $is_show = filter_var($request->getParam("is_show"), FILTER_VALIDATE_INT);

    if (!$image | !$weight | !$link_url || !$is_show) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $slide = new Slide();
    $slide->image = $image;
    $slide->title = $title;
    $slide->link_url = $link_url;
    $slide->remark = $remark;
    $slide->weight = $weight;
    $slide->is_show = $is_show;
    $slide->user_id = $uid;

    $data = $slide->addNewRecord();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//获取幻灯片列表
$app->get('/bms/slide/list', function (Request $request, Response $response, array $args) {


    $page = filter_var($request->getParam("page", 1), FILTER_VALIDATE_INT);
    $order = $request->getParam("order", 'desc');
    $sortBy = $request->getParam("sortBy", 'id');
    $limit = filter_var($request->getParam("limit", 10), FILTER_VALIDATE_INT);
    $uid = filter_var($request->getParam("uid"), FILTER_VALIDATE_INT); //uid

    if (!$page || $page < 1) {
        $page = 1;
    }
    if (!in_array($order, ['desc', 'asc'])) {
        $order = 'desc';
    }

    $slide = new Slide();
    $slide->user_id = $uid;

    $data = $slide->fetchList($page, $limit, $sortBy, $order);

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//获得单条幻灯片记录
$app->get('/bms/slide/detail', function (Request $request, Response $response, array $args) {

    $uid = filter_var($request->getParam("uid"), FILTER_VALIDATE_INT); //uid
    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);


    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "nodata"], null, JSON_UNESCAPED_UNICODE);
    }


    $slide = new Slide();
    $slide->user_id = $uid;
    $slide->id = $id;

    $data = $slide->findOneById();


    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//更新幻灯片记录
$app->post('/bms/slide/update', function (Request $request, Response $response, array $args) {

    $uid = filter_var($request->getParam("uid"), FILTER_VALIDATE_INT); //uid
    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);
    $image = $request->getParam("image");
    $title = $request->getParam("title");
    $link_url = $request->getParam("link_url");
    $remark = $request->getParam("remark");
    $weight = filter_var($request->getParam("weight"), FILTER_VALIDATE_INT);
    $is_show = filter_var($request->getParam("is_show"), FILTER_VALIDATE_INT);

    if (!$image | !$weight | !$link_url || !$is_show) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $slide = new Slide();
    $slide->id = $id;
    $slide->image = $image;
    $slide->title = $title;
    $slide->link_url = $link_url;
    $slide->remark = $remark;
    $slide->weight = $weight;
    $slide->is_show = $is_show;
    $slide->user_id = $uid;


    $data = $slide->updateBy();
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//删除幻灯片
$app->post('/bms/slide/delete', function (Request $request, Response $response, array $args) {

    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);

    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $slide = new Slide();
    $slide->id = $id;
    $data = $slide->deleteById();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});