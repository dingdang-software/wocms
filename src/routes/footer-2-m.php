<?php
/**
 * bms-页脚2
 * User: Lu
 * Date: 2019/2/21
 * Time: 16:03
 */

use Slim\Http\Request;
use Slim\Http\Response;

$app->get("/bms/footer2/list", function (Request $request, Response $response, array $args) {



    $page = filter_var($request->getParam("page", 1), FILTER_VALIDATE_INT);
    $order = $request->getParam("order", 'desc');
    $sortBy = $request->getParam("sortBy", 'id');
    $limit = filter_var($request->getParam("limit", 10), FILTER_VALIDATE_INT);

    if (!$page || $page < 1) {
        $page = 1;
    }
    if (!in_array($order, ['desc', 'asc'])) {
        $order = 'desc';
    }
    $footer2 = new Footer();

    $data = $footer2->fetchAllWithPagination($page, $limit, $sortBy, $order, 'footer_2');

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }

});

$app->post('/bms/footer2/delete', function (Request $request, Response $response, array $args) {

    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);

    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }



    $footer2 = new Footer();
    $footer2->id = $id;
    $data = $footer2->deleteById('footer_2');

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

$app->post('/bms/footer2/add', function (Request $request, Response $response, array $args) {


    $col_id = filter_var($request->getParam("col_id"), FILTER_VALIDATE_INT);
    $col_title = $request->getParam("col_title");
    $link_txt = $request->getParam("link_txt");
    $link_url = $request->getParam("link_url");
    $weight = filter_var($request->getParam("weight"), FILTER_VALIDATE_INT);

    if ( !$col_id || !$link_txt || !$link_url || !$col_title || !$weight) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }




    $footer2 = new Footer();

    $footer2->col_id = $col_id;
    $footer2->col_title = $col_title;
    $footer2->weight = $weight;
    $footer2->link_txt = $link_txt;
    $footer2->link_url = $link_url;

    $data = $footer2->addNewRecord('footer_2');
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

$app->post('/bms/footer2/update', function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);
    $col_id = filter_var($request->getParam("col_id"), FILTER_VALIDATE_INT);
    $col_title = $request->getParam("col_title");
    $link_txt = $request->getParam("link_txt");
    $link_url = $request->getParam("link_url");
    $weight = filter_var($request->getParam("weight"), FILTER_VALIDATE_INT);

    if (!$col_id || !$link_txt || !$link_url || !$col_title || $weight === null || !$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }




    $footer2 = new Footer();
    $footer2->id = $id;
    $footer2->col_id = $col_id;
    $footer2->col_title = $col_title;
    $footer2->weight = $weight;
    $footer2->link_txt = $link_txt;
    $footer2->link_url = $link_url;


    $data = $footer2->updateById('footer_2');
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    }
});