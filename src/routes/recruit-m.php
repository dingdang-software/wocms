<?php
/**
 * bms-招聘
 * User: lu
 * Date: 2018\12\17 0017
 * Time: 10:28
 */

use Slim\Http\Request;
use Slim\Http\Response;


/**
 * 后台管理系统获取所有招聘信息
 */

$app->get("/bms/recruit/list", function (Request $request, Response $response, array $args) {


    $page = filter_var($request->getParam("page", 1), FILTER_VALIDATE_INT);
    $order = $request->getParam("order", 'desc');
    $sortBy = $request->getParam("sortBy", 'id');
    $limit = filter_var($request->getParam("limit", 10), FILTER_VALIDATE_INT);


    if (!$page || $page < 1) {
        $page = 1;
    }
    if (!in_array($order, ['desc', 'asc'])) {
        $order = 'desc';
    }
    $recruit = new Recruit();

    $data = $recruit->fetchList($page, $limit, $sortBy, $order);


    return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);

});

$app->post('/bms/recruit/add', function (Request $request, Response $response, array $args) {


    $job = $request->getParam("job");
    $recruit_type_id = filter_var($request->getParam("recruit_type_id"), FILTER_VALIDATE_INT);
    $recruit_type = $request->getParam("recruit_type");
    $job_nature = $request->getParam("job_nature");
    $salary = $request->getParam("salary");
    $num = $request->getParam("num");
    $work_at = $request->getParam("work_at");
    $skill_requirement = $request->getParam("skill_requirement");
    $published_at = $request->getParam("published_at");
    $expired_at = $request->getParam("expired_at");

    if (!$job | !$recruit_type_id | !$recruit_type || !$job_nature || !$salary || !$num | !$work_at | !$skill_requirement || !$published_at || !$expired_at) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $recruit = new Recruit();
    $recruit->job = $job;
    $recruit->recruit_type_id = $recruit_type_id;
    $recruit->recruit_type = $recruit_type;
    $recruit->job_nature = $job_nature;
    $recruit->salary = $salary;
    $recruit->num = $num;
    $recruit->work_at = $work_at;
    $recruit->skill_requirement = $skill_requirement;
    $recruit->published_at = $published_at;
    $recruit->expired_at = $expired_at;

    $data = $recruit->addNewRecord();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});


$app->get('/bms/recruit/detail', function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);

    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "nodata"], null, JSON_UNESCAPED_UNICODE);
    }


    $recruit = new Recruit();
    $recruit->id = $id;

    $data = $recruit->findOneById();


    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

$app->post('/bms/recruit/update', function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);
    $job = $request->getParam("job");
    $recruit_type_id = filter_var($request->getParam("recruit_type_id"), FILTER_VALIDATE_INT);
    $recruit_type = $request->getParam("recruit_type");
    $job_nature = $request->getParam("job_nature");
    $salary = $request->getParam("salary");
    $num = $request->getParam("num");
    $work_at = $request->getParam("work_at");
    $skill_requirement = $request->getParam("skill_requirement");
    $published_at = $request->getParam("published_at");
    $expired_at = $request->getParam("expired_at");

    if (!$job | !$recruit_type_id | !$recruit_type || !$job_nature || !$salary || !$num | !$work_at | !$skill_requirement || !$published_at || !$expired_at) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $recruit = new Recruit();
    $recruit->id = $id;
    $recruit->job = $job;
    $recruit->recruit_type_id = $recruit_type_id;
    $recruit->recruit_type = $recruit_type;
    $recruit->job_nature = $job_nature;
    $recruit->salary = $salary;
    $recruit->num = $num;
    $recruit->work_at = $work_at;
    $recruit->skill_requirement = $skill_requirement;
    $recruit->published_at = $published_at;
    $recruit->expired_at = $expired_at;


    $data = $recruit->updateBy();
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});


$app->post('/bms/recruit/delete', function (Request $request, Response $response, array $args) {

    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);

    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }


    $recruit = new Recruit();
    $recruit->id = $id;
    $data = $recruit->deleteById();

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});