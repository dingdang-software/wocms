<?php
/**
 * bms-页脚1 api
 * User: Lu
 * Date: 2018/12/29
 * Time: 14:55
 */

use Slim\Http\Request;
use Slim\Http\Response;

//获得页脚1列表
$app->get("/bms/footer1/list", function (Request $request, Response $response, array $args) {



    $page = filter_var($request->getParam("page", 1), FILTER_VALIDATE_INT);
    $order = $request->getParam("order", 'desc');
    $sortBy = $request->getParam("sortBy", 'id');
    $limit = filter_var($request->getParam("limit", 10), FILTER_VALIDATE_INT);

    if (!$page || $page < 1) {
        $page = 1;
    }
    if (!in_array($order, ['desc', 'asc'])) {
        $order = 'desc';
    }
    $footer1 = new Footer();


    $data = $footer1->fetchAllWithPagination($page, $limit, $sortBy, $order, 'footer_1');

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }

});

//更新页脚1信息
$app->post('/bms/footer1/update', function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);
    $display_position = $request->getParam("display_position");
    $link_txt = $request->getParam("link_txt");
    $link_url = $request->getParam("link_url");

    if (!$display_position || !$id || !$link_txt || !$link_url) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }




    $footer1 = new Footer();

    $footer1->id = $id;
    $footer1->display_position = $display_position;
    $footer1->link_txt = $link_txt;
    $footer1->link_url = $link_url;


    $data = $footer1->updateById('footer_1');
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//添加页脚1记录
$app->post('/bms/footer1/add', function (Request $request, Response $response, array $args) {


    $display_position = $request->getParam("display_position");
    $link_txt = $request->getParam("link_txt");
    $link_url = $request->getParam("link_url");

    if ( !$display_position || !$link_txt) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }




    $footer1 = new Footer();

    $footer1->display_position = $display_position;
    $footer1->link_txt = $link_txt;
    $footer1->link_url = $link_url;

    $data = $footer1->addNewRecord('footer_1');
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//删除页脚1记录
$app->post('/bms/footer1/delete', function (Request $request, Response $response, array $args) {

    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);

    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }



    $footer1 = new Footer();
    $footer1->id = $id;
    $data = $footer1->deleteById('footer_1');

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//某一条页脚一记录
$app->get('/bms/footer1/detail', function (Request $request, Response $response, array $args) {

    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);

    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }



    $footer1 = new Footer();
    $footer1->id = $id;
    $data = $footer1->findOneById('footer_1');

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});




