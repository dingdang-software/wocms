<?php
/**
 * bms - 页脚2 列
 * User: Lu
 * Date: 2019/2/21
 * Time: 16:16
 */
use Slim\Http\Request;
use Slim\Http\Response;

//分页获得记录
$app->get("/bms/footer2-col/list", function (Request $request, Response $response, array $args) {



    $page = filter_var($request->getParam("page", 1), FILTER_VALIDATE_INT);
    $order = $request->getParam("order", 'desc');
    $sortBy = $request->getParam("sortBy", 'id');
    $limit = filter_var($request->getParam("limit", 10), FILTER_VALIDATE_INT);


    if (!$page || $page < 1) {
        $page = 1;
    }
    if (!in_array($order, ['desc', 'asc'])) {
        $order = 'desc';
    }
    $footer2Col = new Footer();


    $data = $footer2Col->fetchAllWithPagination($page, $limit, $sortBy, $order, 'footer_2_col');

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }

});

//获得全部记录不分页
$app->get("/bms/footer2-col", function (Request $request, Response $response, array $args) {


    $footer2Col = new Footer();

    $data = $footer2Col->fetchAll('footer_2_col');


    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }

});

//删除记录
$app->post('/bms/footer2-col/delete', function (Request $request, Response $response, array $args) {

    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);

    if (!$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }



    $footer2Col = new Footer();
    $footer2Col->id = $id;
    $data = $footer2Col->deleteById('footer_2_col');

    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//添加记录
$app->post('/bms/footer2-col/add', function (Request $request, Response $response, array $args) {


    $col_title = $request->getParam("col_title");
    $weight = filter_var($request->getParam("weight"), FILTER_VALIDATE_INT);

    if ( !$col_title || !$weight) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }




    $footer2Col = new Footer();
    $footer2Col->col_title = $col_title;
    $footer2Col->weight = $weight;

    $data = $footer2Col->addNewRecord('footer_2_col');
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});

//更新记录
$app->post('/bms/footer2-col/update', function (Request $request, Response $response, array $args) {


    $id = filter_var($request->getParam("id"), FILTER_VALIDATE_INT);
    $col_title = $request->getParam("col_title");
    $weight = filter_var($request->getParam("weight"), FILTER_VALIDATE_INT);

    if ( !$col_title || $weight === null || !$id) {
        return $response->withJson(["success" => false, "msg" => "有数据为空"], null, JSON_UNESCAPED_UNICODE);
    }




    $footer2Col = new Footer();
    $footer2Col->id = $id;
    $footer2Col->col_title = $col_title;
    $footer2Col->weight = $weight;

    $data = $footer2Col->updateById('footer_2_col');
    if ($data) {
        return $response->withJson(["success" => true, "msg" => "成功", 'data' => $data], null, JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson(["success" => false, "msg" => "失败"], null, JSON_UNESCAPED_UNICODE);
    }
});