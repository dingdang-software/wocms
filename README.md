# wocms
wocms网站管理系统，网站内容管理系统，php+mysql开发。

#### 项目介绍

> 《沃~CMS》简称“WOCMS”，是基于PHP（5.2及以上版本） + Mysql 开发的网站管理系统，是一款轻量，简便的网站内容管理系统，可非常灵活的进行二次开发。

#### 软件架构

服务端使用php语言开发，基于slim3.x框架开发的服务端，客户端是H5-PC页面，后台管理系统使用基于react的antd开发程序依赖mysql、php（以及相应的的扩展）

#### PHP安装说明

1. 安装配置好php环境，安装composer，composer依赖于php，请配置好环境变量
2. 项目根目录下执行composer install，如果已经生成composer.lock影响该命令执行，请执行composer update命令
3. 按照自己的服务器配置参数更改server目录中的config目录的文件配置参数，按照注释说明来操作
4. 部署到服务器

#### 后台管理安装说明

1. 进入webpack目录cd webpack，按照自己的服务器配置参数更改webpack目录中的src/components/config目录的请求地址
2. 安装打包工具，如果已经安装请忽略此步骤 
`npm i -g webpack webpack-cli yarn webpack-dev-server`全局安装编译工具  
`npm install`或者`yarn install`安装依赖包  

3. 开发热更新：  
`npm start`或者`npm run dev`

4. 打包生产环境使用文件：
`npm run build:dist`

5. ajv问题参考

#### 客户端安装说明

1. 进入webpack目录cd webpack，执行npm run build进行打包，生成的打包文件在根目录的public目录下
2. 部署到服务器后访问服务器该目录下主页就是本项目客户端的主页

#### 其他：

1. 升级npm版本`npm install -g npm`
2. 指定版本`npm -g install npm@5.6.0`
3. 异常`\node_modules\ajv\lib\keyword.js:65 throw new Error('custom keyword definition is invalid: '  + this.errorsText(validateDefinition.errors));`为版本兼容问题错误，尽量使用一样的版本编译
4. 指定国内淘宝源，提高依赖包下载速度`npm install -g cnpm --registry=http://registry.npm.taobao.org`之后所有用到的 npm 命令都可以使用 cnpm 来代替进行 install。但是 cnpm 不支持 publish 命令，需要注意。
5. 持久更换数据源`npm config set registry https://registry.npm.taobao.org`

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

